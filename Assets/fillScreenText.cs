﻿using UnityEngine;
using System.Collections;

public class fillScreenText : MonoBehaviour {

	
	public float lSize = 4.88f; 
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Camera lCamera = Camera.main;

		int CountSeconds;

		if(lCamera.orthographic)
		{
			float lSizeY = lCamera.orthographicSize * 2.0f;
			float lSizeX = lSizeY *lCamera.aspect;

			float t = 10*(lSizeX/lSize);

			transform.GetComponent<GUIText>().fontSize = (int)t ;
		}
	}
}
