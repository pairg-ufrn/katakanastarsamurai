﻿using UnityEngine;
using System;

[Serializable]
public class Syllable  {

	public static readonly string KEY_KATAKANA = "katakana" ;
	public static readonly string KEY_HIRAGANA = "hiragana" ;
	public static readonly string KEY_ROMAJI = "romaji" ;


	private string katakana;
	private string hiragana;
	private string romaji;

	public int level{
		get{
			return level;
		}
		set{
			level = value;
		}
	}

	public Syllable(string katakana, string hiragana, string romaji, int level){
		this.katakana = katakana;
		this.hiragana = hiragana;
		this.romaji = romaji;
		this.level = level;
	}

	public bool equals (Syllable s){
		return this.romaji == s.romaji;
	}

	public string toString(){
		return romaji;
	}


	public string toString(string key){

		if (key == KEY_HIRAGANA)
			return hiragana;
		if (key == KEY_KATAKANA)
			return katakana;
		else
			return romaji;

	}


}
