﻿using UnityEngine;
using System;


[Serializable]
public class Settings {

	public static readonly string KEY_DIFFICULTY = "DIFFICULTY";
	public static readonly string KEY_MODE = "MODE";
	public static readonly string KEY_LANGUEGE = "LANGUEGE";

	public static readonly string SCENE_MAIN_MENU = "MainMenuScene";
	public static readonly string SCENE_SELECTION_MODE = "SelectionModeScene";
	public static readonly string SCENE_SELECTION_MODEAPPED = "SelectionModeAppedScene";
	public static readonly string SCENE_SELECTION_LEVEL = "SelectionLevelScene";
	public static readonly string SCENE_GAME = "JSGameScene";
	public static readonly string SCENE_RESULT = "ResultScene";
	public static readonly string SCENE_ABOUT = "AboutScene";

	public static readonly string DIFFICULTY_EASY = "easy";
	public static readonly string DIFFICULTY_NORMAL = "normal";
	public static readonly string DIFFICULTY_HARD = "hard";
	public static readonly string MODE_HIRAGANA = "hiragana";
	public static readonly string MODE_ROMAJI = "romaji";


	public string currentDifficulty;
	public string currentMode;
	public string currentLanguege;

}
