﻿using UnityEngine;
using System;


[Serializable]
public class LevelData {

	public static readonly string KEY_LEVEL = "level";
	public static readonly string KEY_RESULT_SUCCESS = "success";
	public static readonly string KEY_RESULT_FULLY_SUCCESS = "fully_success";
	public static readonly string KEY_RESULT_FAIL = "fail";
	public static readonly string KEY_RESULT = "result";

	public enum states {available, closed, cleared, one_star_cleared, two_star_cleared, three_star_cleared};		
	public int index;
	public states state;
	public int maxElementslevel;
	public int enemyCount;
	public int highScore;
	public int lessDamager;



}
