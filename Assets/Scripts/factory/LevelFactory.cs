﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class LevelFactory  {

	private BinaryFormatter bf = new BinaryFormatter ();
	private FileStream file = null;
	private List<LevelData> levelList = null;
	private LevelData ld;
	private List<string> urlFiles;
	private int[] enemyCountList;


	// Use this for initialization
	public LevelFactory () {
	
		urlFiles = new List<string>();

		enemyCountList = new int[25];


		urlFiles.Add(Application.persistentDataPath + "/LD."+Settings.DIFFICULTY_EASY+"."+Settings.MODE_HIRAGANA+".dat");
		urlFiles.Add(Application.persistentDataPath + "/LD."+Settings.DIFFICULTY_HARD+"."+Settings.MODE_HIRAGANA+".dat");
		urlFiles.Add(Application.persistentDataPath + "/LD."+Settings.DIFFICULTY_NORMAL+"."+Settings.MODE_HIRAGANA+".dat");
		urlFiles.Add(Application.persistentDataPath + "/LD."+Settings.DIFFICULTY_EASY+"."+Settings.MODE_ROMAJI+".dat");
		urlFiles.Add(Application.persistentDataPath + "/LD."+Settings.DIFFICULTY_HARD+"."+Settings.MODE_ROMAJI+".dat");
		urlFiles.Add(Application.persistentDataPath + "/LD."+Settings.DIFFICULTY_NORMAL+"."+Settings.MODE_ROMAJI+".dat");

		enemyCountList[0] = 10;
		enemyCountList[1] = 10;  
		enemyCountList[2] = 12;  
		enemyCountList[3] = 12;  
		enemyCountList[4] = 12;  
		enemyCountList[5] = 15;  
		enemyCountList[6] = 15;  
		enemyCountList[7] = 15; 
		enemyCountList[8] = 18;  
		enemyCountList[9] = 18; 
		enemyCountList[10] = 18;  
		enemyCountList[11] = 21;  
		enemyCountList[12] = 21;  
		enemyCountList[13] = 21;  
		enemyCountList[14] = 24;  
		enemyCountList[15] = 24; 
		enemyCountList[16] = 24;  
		enemyCountList[17] = 27;  
		enemyCountList[18] = 27;  
		enemyCountList[19] = 27;  
		enemyCountList[20] = 30;  
		enemyCountList[21] = 30; 
		enemyCountList[22] = 5;  
		enemyCountList[23] = 10;  
		enemyCountList[24] = 15;
		
		foreach (string url in urlFiles){
			if (!File.Exists (url)) {
				
				Debug.Log("entrou para criar");    
				
				file = File.Create (url);
				
				levelList= new List<LevelData>();
				
				ld = new LevelData ();
				ld.index = 1;
				ld.state = LevelData.states.available;
				levelList.Add(ld);
				ld.enemyCount = enemyCountList[0];
				ld.lessDamager = 0;
				ld.highScore = 0;


				for (int i = 1; i < 25; i++) {
					ld = new LevelData ();
					ld.index = i+1;
					ld.enemyCount = enemyCountList[i];
					ld.lessDamager = 0;
					ld.highScore = 0;

					ld.state = LevelData.states.available;
					levelList.Add(ld);
				}
				
				bf.Serialize (file,levelList);

				file.Close ();
			}
		}
	}

	public List<LevelData> getLevels(string DIFFICULTY, string MODE){
		file = File.Open(Application.persistentDataPath + "/LD."+DIFFICULTY+"."+MODE+".dat", FileMode.Open);
		
		levelList = (List<LevelData>) bf.Deserialize(file);
	
		file.Close ();

		return levelList;
	}

	public LevelData getLevel(string DIFFICULTY, string MODE, int index){
		file = File.Open(Application.persistentDataPath + "/LD."+DIFFICULTY+"."+MODE+".dat", FileMode.Open);
		
		levelList = (List<LevelData>) bf.Deserialize(file);
		
		file.Close ();
		
		return levelList[index];
	}

	public void updateLevelData(LevelData level){

	}
	

}
