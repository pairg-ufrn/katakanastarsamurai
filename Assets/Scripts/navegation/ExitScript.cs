﻿using UnityEngine;
using System.Collections;

public class ExitScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Screen.orientation = ScreenOrientation.LandscapeLeft;

		if (PlayerPrefs.GetInt("paused") == 1) {
			Time.timeScale = 1;
			PlayerPrefs.SetInt("paused",0);
		}

		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit(); 
	}

}
