﻿#pragma strict

// Variable to store the enemy prefab
public var enemy : GameObject;

public var bigEnemy : GameObject;

// Variable to know how fast we should create new enemies
public var spawnTime : float = 2.0f;

private var difficulty : String;

function Start() {  
    // Call the 'addEnemy' function every 'spawnTime' seconds
    
    	InvokeRepeating("addEnemy", spawnTime, spawnTime);
  
}

// New function to spawn an enemy
function addEnemy() {  
    // Variables to store the X position of the spawn object
    // See image below
    var x1 = transform.position.x - GetComponent.<Renderer>().bounds.size.x/2;
    var x2 = transform.position.x + GetComponent.<Renderer>().bounds.size.x/2;

    // Randomly pick a point within the spawn object
    var spawnPoint = new Vector2(Random.Range(x1, x2), transform.position.y);

	if (PlayerPrefs.GetInt("level") > 22){
    // Create an enemy at the 'spawnPoint' position
    	if (GameObject.Find("/BigEnemy(Clone)") == null){
    		Instantiate(bigEnemy, spawnPoint, Quaternion.identity);
    	}
    }
    else{
        Instantiate(enemy, spawnPoint, Quaternion.identity);
    }
    
}
