﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class DockBehaviour : MonoBehaviour {

	public GameObject elementDock ;

	public GameController gameController ;

	private List<Vector2> listPosition;
	
	private string difficulty ;
	
	public float spawnTime = 1.2f;

	private int levelToArray ;

	private LevelData level;
	
	private List<bool> listAvaliable ;
	
	private List<GameObject> listElementInDock ;

	private int maxElementslevel;
	
	private int screenWidth  = Screen.width;
	
	private int screenHeight  = Screen.height;

	// Use this for initialization
	void Start () {

		difficulty = PlayerPrefs.GetString(Settings.KEY_DIFFICULTY);
		
		level = gameController.level;
		
		levelToArray = level.index -1;
		
			listElementInDock = new  List<GameObject> ();
		
		listAvaliable = new  List<bool>();
		
		maxElementslevel = level.maxElementslevel;

		listPosition =  new List<Vector2>();								
		
		
		if (maxElementslevel%2==0){
			
			listPosition[0] = new Vector2(1.2f, transform.position.y);	
			listPosition[1] = new Vector2(-1.1f, transform.position.y);
			listPosition[2] = new Vector2(3.5f, transform.position.y);
			listPosition[3] = new Vector2(-3.4f, transform.position.y);
			listPosition[4] = new Vector2(5.8f, transform.position.y);
			listPosition[5] = new Vector2(-5.7f, transform.position.y);
			listPosition[6] = new Vector2(8.1f, transform.position.y);	
			listPosition[7] = new Vector2(-8.0f, transform.position.y);
			listPosition[8] = new Vector2(10.4f, transform.position.y);
			listPosition[9] = new Vector2(-10.3f, transform.position.y);
			listPosition[10] = new Vector2(12.7f, transform.position.y);
			listPosition[11] = new Vector2(-12.6f, transform.position.y);
		}
		else {
			
			listPosition[0] = new Vector2(0f, transform.position.y);	
			listPosition[1] = new Vector2(-2.3f, transform.position.y);
			listPosition[2] = new Vector2(2.3f, transform.position.y);
			listPosition[3] = new Vector2(-4.7f, transform.position.y);
			listPosition[4] = new Vector2(4.6f, transform.position.y);
			listPosition[5] = new Vector2(-6.9f, transform.position.y);
			listPosition[6] = new Vector2(6.9f, transform.position.y);	
			listPosition[7] = new Vector2(-9.2f, transform.position.y);
			listPosition[8] = new Vector2(9.2f, transform.position.y);
			listPosition[9] = new Vector2(-11.5f, transform.position.y);
			listPosition[10] = new Vector2(11.5f, transform.position.y);
			listPosition[11] = new Vector2(-13.8f, transform.position.y);
			
		}
		
		createDock();

	}

	public GameObject createElementInDock(Syllable syllable){
			int i;
			
			do{
				i = Random.Range(0, maxElementslevel);
				
			}while(!listAvaliable[i]);
			
			listAvaliable[i] = false;                 

			listElementInDock[i].GetComponent<ButtonGameBehaviour>().unSeleted();

			GameObject textChild = listElementInDock[i].transform.FindChild("text").gameObject;
						
			TextMesh tm = textChild.GetComponent<TextMesh>();
			
			tm.text = syllable.toString(PlayerPrefs.GetString(Settings.KEY_MODE));
			
			createFake();   
			
			return listElementInDock[i];
	}

	public void createFake() {  
			
			int  i ;
			
			for (i = 0; i < maxElementslevel; i++){
				if(listAvaliable[i]){
					
					Syllable s = GameObject.Find("/GameController").GetComponent<GameController>().getSyllable();
					
					GameObject textChild = listElementInDock[i].transform.FindChild("text").gameObject;
					
					listElementInDock[i].GetComponent<ButtonGameBehaviour>().unSeleted();
					
					TextMesh tm = textChild.GetComponent<TextMesh>();
					
					tm.text = s.toString(PlayerPrefs.GetString(Settings.KEY_MODE));
					
					//	   	listElementInDock[i].GetComponent(ButtonBehaviourScript).dieTime();
				}
			}
		}

	public void createDock(){
		
		int i;
		
		Transform textChild = elementDock.transform.FindChild("text");
		
		TextMesh tm = textChild.GetComponent<TextMesh>();
		
		tm.text = "";
		
		for (i = 0; i < 12; i++){
			if (listElementInDock[i] == null){
				listElementInDock[i] = Instantiate(elementDock,listPosition[i], Quaternion.identity) as GameObject;
				listElementInDock[i].transform.parent = transform;
				listElementInDock[i].GetComponent<ButtonGameBehaviour>().hidden();
				listElementInDock[i].GetComponent<ButtonGameBehaviour>().indexInList = i;
				listAvaliable[i] = true;
			}
		}
		
	}

	// Update is called once per frame
	void Update () {
			createDock ();
	}

	public void unBlockButton( int index ){
		listAvaliable[index] = true;
	} 
	
	public void  blockButton( int index  ){
		listAvaliable[index] = false;
	} 


}
}