﻿#pragma strict

// Public variable that contains the speed of the enemy
public var speed : float = 0.1f;

private var texts : String[];

public var timelive : int = 10;

public var explosion : GameObject;

private var katakanaCharacteres : String[];

private var hiraganaCharacteres : String[];

private var romajiCharacteres : String[];

public var button: GameObject[];

var platform : RuntimePlatform = Application.platform;

private var starTransform : Transform;

private var indexC:int;

public var spriteSeleted : Sprite;

public var spriteUnseleted : Sprite;

// Function called when the enemy is created
function Start () {  
    
    // Make the enemy rotate on itself
    
	var textChild1 = transform.Find("text1");
	var textChild2 = transform.Find("text2");
	var textChild3 = transform.Find("text3");
	var textChild4 = transform.Find("text4");
	var textChild5 = transform.Find("text5");

		
	
	var tm1 = textChild1.GetComponent(TextMesh);
	var tm2 = textChild2.GetComponent(TextMesh);
	var tm3 = textChild3.GetComponent(TextMesh);
	var tm4 = textChild4.GetComponent(TextMesh);
	var tm5 = textChild5.GetComponent(TextMesh);
	
	katakanaCharacteres = new Array (5);

	button = new Array (5);

	hiraganaCharacteres = new Array (5);

	romajiCharacteres = new Array (5);	
	
	for (var i = 0; i < 5; i++){					
		indexC = GameObject.Find("/GameController").GetComponent(GameController).getIndexCharacters();
		katakanaCharacteres[i] = GameObject.Find("/GameController").GetComponent(GameController).getKatakanaIndex(indexC);
		hiraganaCharacteres[i] = GameObject.Find("/GameController").GetComponent(GameController).getHiraganaIndex(indexC);
	    romajiCharacteres[i] = GameObject.Find("/GameController").GetComponent(GameController).getRomajiIndex(indexC);


		if (PlayerPrefs.GetString("modo") == "romaji"){        
		    button[i] = GameObject.Find("/Dock").GetComponent(DockBehaviourScript).createElementInDock(romajiCharacteres[i]);
	    }
	    else{
	    	
	   	    button[i] = GameObject.Find("/Dock").GetComponent(DockBehaviourScript).createElementInDock(hiraganaCharacteres[i]);
	    }

	
	}
	          	          
    tm1.text = katakanaCharacteres[0];
    tm2.text = katakanaCharacteres[1];
    tm3.text = katakanaCharacteres[2];
    tm4.text = katakanaCharacteres[3];
    tm5.text = katakanaCharacteres[4];
        
    tm1.characterSize = 0.25;
    tm2.characterSize = 0.25;    
    tm3.characterSize = 0.25;    
    tm4.characterSize = 0.25;   
    tm5.characterSize = 0.25;
         
    starTransform = GameObject.Find("/star").transform;
   
 	//rigidbody2D.angularVelocity = Mathf.Sign(starTransform.position.x - transform.position.x) ;


	GetComponent.<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
    
    
    // Destroy the enemy in 3 seconds,
    // when it is no longer visible on the screen
    Destroy(gameObject, timelive);
}

function Update () {

if(PlayerPrefs.GetInt("paused") == 0){
    //rigidbody2D.velocity.x = Mathf.Sign(starTransform.position.x - transform.position.x) ;

//	var step = speed * Time.deltaTime;
		
	 if (PlayerPrefs.GetString("difficulty") == "easy"){        
	   speed = 0.01;
    }
    else if (PlayerPrefs.GetString("difficulty") == "hard"){
    	speed = 0.1;
    }
    else{
    	 speed = 0.05;
    }
	
	
	    		
	// Move our position a step closer to the target.
<<<<<<< HEAD
	if (starTransform != null){
		
	
			transform.position = Vector3.MoveTowards(transform.position, starTransform.position, speed);
	//		GetComponent.<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
    }
=======
	transform.position = Vector3.MoveTowards(transform.position, starTransform.position, speed);

	GetComponent.<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
    
>>>>>>> 7935a6c2999da4b3ee2f3e0f4342dfb6acc0f989

	var hitCollider : boolean;


	if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player){
           if(Input.touchCount > 0) {
             if(Input.GetTouch(0).phase == TouchPhase.Began){
                  var touchPosition : Vector2 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
		         hitCollider = gameObject.GetComponent(BoxCollider2D).OverlapPoint(touchPosition);
		 
		        
		 
		 
		         if(hitCollider){
		             checkTouch();    
		         }
             }
         }
     }else if(platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer){
         if (Input.GetMouseButtonDown(0))
     {
         

         var mousePosition : Vector2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
         hitCollider = gameObject.GetComponent(BoxCollider2D).OverlapPoint(mousePosition);
 
         Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    
 
 
         if(hitCollider){
             checkTouch();    
         }
     } 

	}
	
	if (button == null){
		Destroy(gameObject);
	}
}

}

function checkTouch(){
      GameObject.Find("/GameController").GetComponent(GameController).registerEnemy(gameObject);
 }




// Function called when the enemy collides with another object
function OnTriggerEnter2D(obj : Collider2D) {  
    var name = obj.gameObject.name;

    // If it collided with a bullet
    if (name == "bullet(Clone)") {
        // Destroy itself (the enemy)
             
        die();

		GameObject.Find("/GameController").GetComponent(GameController).AddScore(1);

        // And destroy the bullet
        Destroy(obj.gameObject);
    }     
    
    // If collide with star
    if (name == "star"){
    	//destroy itself
    	die();
    	// take hp
    	obj.gameObject.GetComponent(StarBehaviourScript).TakeDamage(1);
    	
    }

    // If it collided with the spaceship
    if (name == "spaceship") {
        // Destroy itself (the enemy) to keep things simple
        die();
    }
}

function seleted(){
	
	transform.GetComponent(SpriteRenderer).color = Color.red;

}

function unSeleted(){
	
	transform.GetComponent(SpriteRenderer).color = Color.white;

}

function die(){
	Instantiate(explosion, transform.position, transform.rotation);
	
	button[0].GetComponent(ButtonBehaviourScript).die();
	button[1].GetComponent(ButtonBehaviourScript).die();
	button[2].GetComponent(ButtonBehaviourScript).die();	
	button[3].GetComponent(ButtonBehaviourScript).die();
	button[4].GetComponent(ButtonBehaviourScript).die();
	
	Destroy(gameObject);

}