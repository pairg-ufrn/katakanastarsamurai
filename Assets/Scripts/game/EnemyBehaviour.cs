﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {

	public float speed = 0.01f;
	
	private Syllable syllable ;
	
	public int timelive  = 5;
	
	public GameObject explosion;
	

	public GameObject button;
	
	private RuntimePlatform platform = Application.platform;
	
	private Transform starTransform ;
	
	private int indexC;
	
	public Sprite spriteSeleted ;
	
	public Sprite spriteUnseleted ;
	
	public bool paused ;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetString("difficulty") == "easy"){        
			speed = 0.05f;
		}
		else if (PlayerPrefs.GetString("difficulty") == "hard"){
			speed = 0.12f;
		}
		else{
			speed = 0.09f;
		}
		
		paused = false;
		
		// Make the enemy rotate on itself
		
		GameObject textChild = transform.Find("text").gameObject;
		
		TextMesh tm = textChild.GetComponent<TextMesh>();
		
		syllable = GameObject.Find("/GameController").GetComponent<GameController>().getSyllable();
		
		tm.text = syllable.toString(Syllable.KEY_KATAKANA);
		
		tm.characterSize = 0.25f;
				
		button = GameObject.Find("/Dock").GetComponent<DockBehaviour>().createElementInDock(syllable);
				
		starTransform = GameObject.Find("/star").transform;
		
		//rigidbody2D.angularVelocity = Mathf.Sign(starTransform.position.x - transform.position.x) ;
		
		
		GetComponent<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );

		
		// Destroy the enemy in 3 seconds,
		// when it is no longer visible on the screen
		Destroy(gameObject, timelive);
	}

	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetInt("paused") == 0){
			//rigidbody2D.velocity.x = Mathf.Sign(starTransform.position.x - transform.position.x) ;
			
			//	var step = speed * Time.deltaTime;
			
			// Move our position a step closer to the target.
			if (starTransform != null){
				
				transform.position = Vector3.MoveTowards(transform.position, starTransform.position, speed);
				GetComponent<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
			}
			
				bool hitCollider;
				
				if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player) {
					if (Input.touchCount > 0) {
						if (Input.GetTouch (0).phase == TouchPhase.Began) {
							Vector2 touchPosition = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
							hitCollider = gameObject.GetComponent<BoxCollider2D> ().OverlapPoint (touchPosition);
							
							if (hitCollider) {
								checkTouch();
							}
						}
					}
				} else if (platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer || platform == RuntimePlatform.WindowsWebPlayer || platform == RuntimePlatform.WindowsPlayer) {
					if (Input.GetMouseButtonDown (0)) {
						Vector2 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
						hitCollider = gameObject.GetComponent<BoxCollider2D> ().OverlapPoint (mousePosition);
						
						Debug.Log ("mouse pos " + mousePosition.x + " y " + mousePosition.y + " ");    
						
						if (hitCollider) {
							checkTouch();
						}
					}
					
				}
		}
		
	}
	
	public void checkTouch(){
		GameObject.Find("/GameController").GetComponent<GameController>().registerEnemy(gameObject);
	}
	
	
	
	
	// Function called when the enemy collides with another object
	public void OnTriggerEnter2D(Collider2D obj ) {  
		string name = obj.gameObject.name;
		
		// If it collided with a bullet
		if (name == "bullet(Clone)") {
			// Destroy itself (the enemy)
			
			die();
			
			GameObject.Find("/GameController").GetComponent<GameController>().AddScore(1);
			
			// And destroy the bullet
			Destroy(obj.gameObject);
		}     
		
		// If collide with star
		if (name == "star"){
			//destroy itself
			die();
			// take hp
			obj.gameObject.GetComponent<StarBehaviour>().TakeDamage(1);
			
		}
		
		// If it collided with the spaceship
		if (name == "spaceship") {
			// Destroy itself (the enemy) to keep things simple
			die();
		}
	}
	
	public void seleted(){
		
		transform.GetComponent<SpriteRenderer>().color = Color.red;
		
	}
	
	public void unSeleted(){
		
		transform.GetComponent<SpriteRenderer>().color = Color.white;
		
	}
	
	public void die(){
		Instantiate(explosion, transform.position, transform.rotation);
		if (button != null){
			button.GetComponent<ButtonGameBehaviour>().die();
		}
		Destroy(gameObject);
		
	}
}
}