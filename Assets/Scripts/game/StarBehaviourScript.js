﻿#pragma strict

//Variable that contains the star's life
public var life : int = 5;
public var hit : boolean = false;

public var explosion : GameObject;


function Start () {

}

function Update () {

	transform.GetComponent(Animator).SetInteger("life",life);
		
	
}

function TakeDamage(dmg:int) {
        life -= dmg;

        Instantiate(explosion, transform.position, transform.rotation);
		        
        Debug.Log(gameObject.name + " has " + life + " health");
}

function Die() {
        Debug.Log(gameObject.name + " is dead");
        
        Instantiate(explosion, transform.position, transform.rotation);
        
        GameObject.Find("/GameController").GetComponent(GameController).GameOver();
        
        Destroy(gameObject);
}