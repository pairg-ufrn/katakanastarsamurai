﻿#pragma strict

var platform : RuntimePlatform = Application.platform;

public var hiddened : boolean = false; 

public var indexInList: int;

public var enemy : GameObject;

function Start () {
//transform.GetComponent(Animator).SetInteger("state",1);

	
}

function Update () {

	var hitCollider : boolean;

	if(!hiddened){

		if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player){
	         if(Input.touchCount > 0) {
	             if(Input.GetTouch(0).phase == TouchPhase.Began){
	                  var touchPosition : Vector2 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			         hitCollider = gameObject.GetComponent(BoxCollider2D).OverlapPoint(touchPosition);			 
			         if(hitCollider){
			             checkTouch();
			              
			         }
	             }
	         }
	     }else if(platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer){
	         if (Input.GetMouseButtonDown(0)){
		         var mousePosition : Vector2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		         hitCollider = gameObject.GetComponent(BoxCollider2D).OverlapPoint(mousePosition);
		 
		         Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    
		 
		 
		         if(hitCollider){
			      
		             checkTouch();    
		         }
	    	} 

		}
	}
	

}

function block(){
	GameObject.Find("/Dock").GetComponent(DockBehaviourScript).blockButton(indexInList);
}

function unblock(){
	GameObject.Find("/Dock").GetComponent(DockBehaviourScript).unBlockButton(indexInList);
}



function checkTouch(){
      GameObject.Find("/GameController").GetComponent(GameController).registerButton(gameObject);
      block();
}

function hidden(){
	
	hiddened = true;
		
	transform.GetComponent(Animator).SetInteger("state",-1);

}

function seleted(){

	hiddened = false;
	
   Debug.Log("seleted");    	
		
	transform.GetComponent(Animator).SetInteger("state",1);

}

function unSeleted(){
	
	hiddened = false;
	transform.GetComponent(Animator).SetInteger("state",0);
	unblock();
}
function die(){
	Debug.Log("Dead B "+	transform.FindChild("text").GetComponent(TextMesh).text);
	Destroy(gameObject);
	
}