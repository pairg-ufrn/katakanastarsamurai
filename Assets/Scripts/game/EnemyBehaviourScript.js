﻿#pragma strict

// Public variable that contains the speed of the enemy
public var speed : float = 0.01f;

private var texts : String[];

public var timelive : int = 5;

public var explosion : GameObject;

private var katakanaCharactere : String;

private var hiraganaCharactere : String;

private var romajiCharactere : String;

public var button: GameObject;

var platform : RuntimePlatform = Application.platform;

private var starTransform : Transform;

private var indexC:int;

public var spriteSeleted : Sprite;

public var spriteUnseleted : Sprite;

public static var KEY_LEVEL  : String = "level" ;
public static var KEY_RESULT_SUCCESS  : String = "success" ;
public static var KEY_RESULT_FULLY_SUCCESS  : String = "fully_success" ;
public static var KEY_RESULT_FAIL  : String = "fail" ;
public static var KEY_RESULT  : String = "result" ;

public static  var KEY_DIFFICULTY : String  = "DIFFICULTY";
public static  var KEY_MODE : String  = "MODE";
public static  var KEY_LANGUEGE : String  = "LANGUEGE";

public static  var DIFFICULTY_EASY : String = "easy";
public static  var DIFFICULTY_NORMAL : String = "normal";
public static  var DIFFICULTY_HARD : String = "hard";
public static  var MODE_HIRAGANA : String = "hiragana";
public static  var MODE_ROMAJI : String = "romaji";

public var paused : boolean;

// Function called when the enemy is created
function Start () {  
    
    if (PlayerPrefs.GetString("difficulty") == "easy"){        
	   GetComponent.<Rigidbody2D>().angularDrag = 100;
    }
    else if (PlayerPrefs.GetString("difficulty") == "hard"){
    	GetComponent.<Rigidbody2D>().angularDrag = 0;
    }
    else{
    	GetComponent.<Rigidbody2D>().angularDrag = 10;
    }
    
    paused = false;
    
    // Make the enemy rotate on itself
    
	var textChild = transform.Find("text");
	
	var tm = textChild.GetComponent(TextMesh);
	
	indexC = GameObject.Find("/GameController").GetComponent(GameController).getIndexCharacters();
	
	katakanaCharactere = GameObject.Find("/GameController").GetComponent(GameController).getKatakanaIndex(indexC);
	
	//Debug.Log(katakanaCharactere);          
	          
    tm.text = katakanaCharactere;
    
    //tm.characterSize = 0.25;
    
    hiraganaCharactere = GameObject.Find("/GameController").GetComponent(GameController).getHiraganaIndex(indexC);
    
    //Debug.Log(hiraganaCharactere);
    
    romajiCharactere = GameObject.Find("/GameController").GetComponent(GameController).getRomajiIndex(indexC);
    
    Debug.Log("Kata "+katakanaCharactere+" Hira "+hiraganaCharactere +" Roma "+ romajiCharactere);
    
    
    if (PlayerPrefs.GetString(KEY_MODE) == MODE_ROMAJI){           
	    button = GameObject.Find("/Dock").GetComponent(DockBehaviourScript).createElementInDock(romajiCharactere);
    }
    else{
   	    button = GameObject.Find("/Dock").GetComponent(DockBehaviourScript).createElementInDock(hiraganaCharactere);
    }
    
    button.GetComponent(ButtonBehaviourScript).enemy = gameObject;
    
    starTransform = GameObject.Find("/star").transform;
   
 	//rigidbody2D.angularVelocity = Mathf.Sign(starTransform.position.x - transform.position.x) ;


	GetComponent.<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
    
    
    // Destroy the enemy in 3 seconds,
    // when it is no longer visible on the screen
    Destroy(gameObject, timelive);
}


function Update () {

if(PlayerPrefs.GetInt("paused") == 0){
    //rigidbody2D.velocity.x = Mathf.Sign(starTransform.position.x - transform.position.x) ;

//	var step = speed * Time.deltaTime;
		
	 if (PlayerPrefs.GetString("difficulty") == "easy"){        
	   speed = 0.01;
    }
    else if (PlayerPrefs.GetString("difficulty") == "hard"){
    	speed = 0.1;
    }
    else{
    	 speed = 0.05;
    }
	
	
	    		
	// Move our position a step closer to the target.
	if (starTransform != null){
		
<<<<<<< HEAD
	
			transform.position = Vector3.MoveTowards(transform.position, starTransform.position, speed);
	//		GetComponent.<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
=======
		transform.position = Vector3.MoveTowards(transform.position, starTransform.position, speed);
		GetComponent.<Rigidbody2D>().angularVelocity =  Mathf.MoveTowardsAngle(starTransform.position.x - transform.position.x,starTransform.position.y - transform.position.y, starTransform.position.z - transform.position.z );
>>>>>>> 7935a6c2999da4b3ee2f3e0f4342dfb6acc0f989
    }

	var hitCollider : boolean;


	if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player){
           if(Input.touchCount > 0) {
             if(Input.GetTouch(0).phase == TouchPhase.Began){
                  var touchPosition : Vector2 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
		         hitCollider = gameObject.GetComponent(BoxCollider2D).OverlapPoint(touchPosition);
		 
		        
		 
		 
		         if(hitCollider){
		             checkTouch();    
		         }
             }
         }
     }else if(platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer){
         if (Input.GetMouseButtonDown(0))
     {
         

         var mousePosition : Vector2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
         hitCollider = gameObject.GetComponent(BoxCollider2D).OverlapPoint(mousePosition);
 
         Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    
 
 
         if(hitCollider){
             checkTouch();    
         }
     } 

	}
	
	if (button == null){
		Destroy(gameObject);
	}
}

}

function checkTouch(){
      GameObject.Find("/GameController").GetComponent(GameController).registerEnemy(gameObject);
 }




// Function called when the enemy collides with another object
function OnTriggerEnter2D(obj : Collider2D) {  
    var name = obj.gameObject.name;

    // If it collided with a bullet
    if (name == "bullet(Clone)") {
        // Destroy itself (the enemy)
             
        die();

		GameObject.Find("/GameController").GetComponent(GameController).AddScore(1);

        // And destroy the bullet
        Destroy(obj.gameObject);
    }     
    
    // If collide with star
    if (name == "star"){
    	//destroy itself
    	die();
    	// take hp
    	obj.gameObject.GetComponent(StarBehaviourScript).TakeDamage(1);
    	
    }

    // If it collided with the spaceship
    if (name == "spaceship") {
        // Destroy itself (the enemy) to keep things simple
        die();
    }
}

function seleted(){
	
	transform.GetComponent(SpriteRenderer).color = Color.red;

}

function unSeleted(){
	
	transform.GetComponent(SpriteRenderer).color = Color.white;

}

function die(){
	Instantiate(explosion, transform.position, transform.rotation);
	if (button != null){
		button.GetComponent(ButtonBehaviourScript).die();
		
	}
	Destroy(gameObject);

}