﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

	public class ButtonGameBehaviour : MonoBehaviour {

		
		private RuntimePlatform platform = Application.platform;
		

		public Sprite spriteSeleted ;
		
		public Sprite spriteUnseleted ;
		
		public bool hiddened  = false; 
		
		public int indexInList;

		public LevelData level;
		public Syllable syllable;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
				bool hitCollider;
				
				if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player) {
					if (Input.touchCount > 0) {
						if (Input.GetTouch (0).phase == TouchPhase.Began) {
							Vector2 touchPosition = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
							hitCollider = gameObject.GetComponent<BoxCollider2D> ().OverlapPoint (touchPosition);
							
							if (hitCollider) {
								checkTouch();
							}
						}
					}
			} else if (platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer || platform == RuntimePlatform.WindowsWebPlayer || platform == RuntimePlatform.WindowsPlayer) {
					if (Input.GetMouseButtonDown (0)) {
						Vector2 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
						hitCollider = gameObject.GetComponent<BoxCollider2D> ().OverlapPoint (mousePosition);
						
						Debug.Log ("mouse pos " + mousePosition.x + " y " + mousePosition.y + " ");    
						
						if (hitCollider) {
							checkTouch();
						}
					}
					
				}
		}


		public void block(){
			GameObject.Find("/Dock").GetComponent<DockBehaviour>().blockButton(indexInList);
		}
		
		public void unblock(){
			GameObject.Find("/Dock").GetComponent<DockBehaviour>().unBlockButton(indexInList);
		}
		
		

		public void checkTouch(){
			GameObject.Find("/GameController").GetComponent<GameController>().registerButton(gameObject);
			block();
		}
		
		public void hidden(){
			
			hiddened = true;
			
			transform.FindChild("spriteButton").GetComponent<SpriteRenderer>().sprite =	null;
			
		}
		
		public void seleted(){
			
			hiddened = false;
			
			transform.FindChild("spriteButton").GetComponent<SpriteRenderer>().sprite =	spriteSeleted;
			
		}
		
		public void unSeleted(){
			
			hiddened = false;
			
			transform.FindChild("spriteButton").GetComponent<SpriteRenderer>().sprite = 	spriteUnseleted;
			unblock();
		}

		public void die(){
			Destroy(gameObject);
		}


	}
}