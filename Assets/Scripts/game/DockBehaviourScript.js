﻿#pragma strict

public var elementDock : GameObject;

private var listPosition : Vector2[];

private var difficulty : String ;

public var spawnTime : float = 1.2f;

private var level : int;

private var levelToArray : int;

private var maxElementslevel : int[];

private var listAvaliable : boolean[];

private var listElementInDock : GameObject[];

private var screenWidth :int = Screen.width;

private var screenHeight :int = Screen.height;

public static var KEY_LEVEL  : String = "level" ;
public static var KEY_RESULT_SUCCESS  : String = "success" ;
public static var KEY_RESULT_FULLY_SUCCESS  : String = "fully_success" ;
public static var KEY_RESULT_FAIL  : String = "fail" ;
public static var KEY_RESULT  : String = "result" ;

public static  var KEY_DIFFICULTY : String  = "DIFFICULTY";
public static  var KEY_MODE : String  = "MODE";
public static  var KEY_LANGUEGE : String  = "LANGUEGE";

public static  var DIFFICULTY_EASY : String = "easy";
public static  var DIFFICULTY_NORMAL : String = "normal";
public static  var DIFFICULTY_HARD : String = "hard";
public static  var MODE_HIRAGANA : String = "hiragana";
public static  var MODE_ROMAJI : String = "romaji";

function Start () {
	
	difficulty = PlayerPrefs.GetString(KEY_DIFFICULTY);
						
	level = PlayerPrefs.GetInt(KEY_LEVEL);
	
	levelToArray = level -1;
	
	listElementInDock = new Array(12);
	
	listAvaliable = new Array(12);
				
	maxElementslevel = new Array(25);

	maxElementslevel[0] = 5;
	maxElementslevel[1] = 5;
	maxElementslevel[2] = 5;
	maxElementslevel[3] = 6;
	maxElementslevel[4] = 6;
	maxElementslevel[5] = 6;
	maxElementslevel[6] = 7;
	maxElementslevel[7] = 7;
	maxElementslevel[8] = 7;
	maxElementslevel[9] = 8;
	maxElementslevel[10] = 8;
	maxElementslevel[11] = 8;
	maxElementslevel[12] = 9;
	maxElementslevel[13] = 9;
	maxElementslevel[14] = 9;
	maxElementslevel[15] = 10;
	maxElementslevel[16] = 10;
	maxElementslevel[17] = 10;
	maxElementslevel[18] = 11;
	maxElementslevel[19] = 11;
	maxElementslevel[20] = 11;
	maxElementslevel[21] = 12;
	maxElementslevel[22] = 12;
	maxElementslevel[23] = 12;
	maxElementslevel[24] = 12;

	listPosition = new Array(12);								


	/*if (maxElementslevel[levelToArray]%2==0){

		listPosition[0] = new Vector2(1.2f, transform.position.y);	
		listPosition[1] = new Vector2(-1.1f, transform.position.y);
		listPosition[2] = new Vector2(3.5f, transform.position.y);
		listPosition[3] = new Vector2(-3.4f, transform.position.y);
		listPosition[4] = new Vector2(5.8f, transform.position.y);
		listPosition[5] = new Vector2(-5.7f, transform.position.y);
		listPosition[6] = new Vector2(8.1f, transform.position.y);	
		listPosition[7] = new Vector2(-8.0f, transform.position.y);
		listPosition[8] = new Vector2(10.4f, transform.position.y);
		listPosition[9] = new Vector2(-10.3f, transform.position.y);
		listPosition[10] = new Vector2(12.7f, transform.position.y);
		listPosition[11] = new Vector2(-12.6f, transform.position.y);
	}
	else {
	
		listPosition[0] = new Vector2(0f, transform.position.y);	
		listPosition[1] = new Vector2(-2.3f, transform.position.y);
		listPosition[2] = new Vector2(2.3f, transform.position.y);
		listPosition[3] = new Vector2(-4.7f, transform.position.y);
		listPosition[4] = new Vector2(4.6f, transform.position.y);
		listPosition[5] = new Vector2(-6.9f, transform.position.y);
		listPosition[6] = new Vector2(6.9f, transform.position.y);	
		listPosition[7] = new Vector2(-9.2f, transform.position.y);
		listPosition[8] = new Vector2(9.2f, transform.position.y);
		listPosition[9] = new Vector2(-11.5f, transform.position.y);
		listPosition[10] = new Vector2(11.5f, transform.position.y);
		listPosition[11] = new Vector2(-13.8f, transform.position.y);
	
	}*/
	
	if (maxElementslevel[levelToArray]%2==0){

		listPosition[0] = new Vector2(1.8f, transform.position.y);	
		listPosition[1] = new Vector2(-1.65f, transform.position.y);
		listPosition[2] = new Vector2(5.25f, transform.position.y);
		listPosition[3] = new Vector2(-5.1f, transform.position.y);
		listPosition[4] = new Vector2(8.7f, transform.position.y);
		listPosition[5] = new Vector2(-8.55f, transform.position.y);
		listPosition[6] = new Vector2(12.15f, transform.position.y);	
		listPosition[7] = new Vector2(-12.0f, transform.position.y);
		listPosition[8] = new Vector2(15.6f, transform.position.y);
		listPosition[9] = new Vector2(-15.45f, transform.position.y);
		listPosition[10] = new Vector2(19.05f, transform.position.y);
		listPosition[11] = new Vector2(-18.9f, transform.position.y);
	}
	else {
	
		listPosition[0] = new Vector2(0f, transform.position.y);	
		listPosition[1] = new Vector2(-3.45f, transform.position.y);
		listPosition[2] = new Vector2(3.45f, transform.position.y);
		listPosition[3] = new Vector2(-7.05f, transform.position.y);
		listPosition[4] = new Vector2(7.05f, transform.position.y);
		listPosition[5] = new Vector2(-10.35f, transform.position.y);
		listPosition[6] = new Vector2(10.35f, transform.position.y);	
		listPosition[7] = new Vector2(-13.8f, transform.position.y);
		listPosition[8] = new Vector2(13.8f, transform.position.y);
		listPosition[9] = new Vector2(-17.25f, transform.position.y);
		listPosition[10] = new Vector2(17.25f, transform.position.y);
		listPosition[11] = new Vector2(-20.7f, transform.position.y);
	
	}
				
	createDock();


}

function addFake() {  
    // Variables to store the X position of the spawn object
    // See image below
    
	var  i : int;
	
	do{
    	 i = Random.Range(0, maxElementslevel[levelToArray]);
    	 	 
    }while(listElementInDock[i].transform.FindChild("text").GetComponent(TextMesh).text != "");
    
    var indexC = GameObject.Find("/GameController").GetComponent(GameController).getIndexCharacters();
	
   	var textChild = listElementInDock[i].transform.FindChild("text");
    
    listElementInDock[i].GetComponent(ButtonBehaviourScript).unSeleted();
    
    var tm = textChild.GetComponent(TextMesh);

	if (PlayerPrefs.GetString(KEY_MODE) == MODE_ROMAJI){        
    	tm.text = GameObject.Find("/GameController").GetComponent(GameController).getRomajiIndex(indexC);
    }
    else{
   		 tm.text = GameObject.Find("/GameController").GetComponent(GameController).getHiraganaIndex(indexC);
    }
   
    
}

function createFake() {  
    
	var  i : int;

	for (i = 0; i < maxElementslevel[levelToArray]; i++){
		if (listElementInDock[i].GetComponent(ButtonBehaviourScript).enemy == null){
			var textChild = listElementInDock[i].transform.FindChild("text");
			    
		    listElementInDock[i].GetComponent(ButtonBehaviourScript).unSeleted();

		    var tm = textChild.GetComponent(TextMesh);
			
			
			if(tm.text == ""){
				  
					
			    var indexC = GameObject.Find("/GameController").GetComponent(GameController).getIndexCharacters();
		
				if (PlayerPrefs.GetString(KEY_MODE) == MODE_ROMAJI){      
			    	tm.text = GameObject.Find("/GameController").GetComponent(GameController).getRomajiIndex(indexC);
			    }
			    else{
			   		 tm.text = GameObject.Find("/GameController").GetComponent(GameController).getHiraganaIndex(indexC);
			    }
			   
		//	   	listElementInDock[i].GetComponent(ButtonBehaviourScript).dieTime();
			}
    }
    }
}

function unBlockButton( index : int ){
	listAvaliable[index] = true;
} 

function blockButton( index : int ){
	listAvaliable[index] = false;
} 

function createElementInDock(character : String){
	var  i : int;

    
	
	do{
    	 i = Random.Range(0, maxElementslevel[levelToArray]);
    	 	 
    }while(!listAvaliable[i]);
    
   
	
    
    listAvaliable[i] = false;                 


                                                                  
    var textChild = listElementInDock[i].transform.FindChild("text");
        
    listElementInDock[i].GetComponent(ButtonBehaviourScript).unSeleted();
    
    var tm = textChild.GetComponent(TextMesh);
    
    tm.text = character;
 	
 	createFake();   
    
    return listElementInDock[i];
}


function createDock(){
	
	var i : int;
	
	var textChild = elementDock.transform.FindChild("text");
    
    var tm = textChild.GetComponent(TextMesh);
    
    tm.text = "";

	for (i = 0; i < 12; i++){
		if (listElementInDock[i] == null){
			listElementInDock[i] = Instantiate(elementDock,listPosition[i], Quaternion.identity);
			listElementInDock[i].transform.parent = transform;
			listElementInDock[i].GetComponent(ButtonBehaviourScript).hidden();
			listElementInDock[i].GetComponent(ButtonBehaviourScript).indexInList = i;
			listAvaliable[i] = true;
		}
	}

}


function Update () {
	createDock();

}