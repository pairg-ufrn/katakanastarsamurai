﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

public class StarBehaviour : MonoBehaviour {

	public int life  = 5;
	public bool hit  = false;
	
	public GameObject explosion ;
	
	public Sprite lifeFull ; 
	public Sprite lifeLessOne ; 
	public Sprite lifeLessTwo ; 
	public Sprite lifeLessThree ;
	public Sprite lifeLessFour ;  


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		switch(life){
			
		case 1:
			transform.GetComponent<SpriteRenderer>().sprite = lifeLessFour;
			break;
			
		case 2:
			transform.GetComponent<SpriteRenderer>().sprite = lifeLessThree;	
			break;
			
		case 3:
			transform.GetComponent<SpriteRenderer>().sprite = lifeLessTwo;
			break;
			
		case 4:
            transform.GetComponent<SpriteRenderer>().sprite = lifeLessOne;
			break;
			
		case 5:
			transform.GetComponent<SpriteRenderer>().sprite = lifeFull;
			
			break;
			
		}

	}


	public void TakeDamage(int dmg) {
		life -= dmg;
		
		Instantiate(explosion, transform.position, transform.rotation);
		
		if (life <= 0) {
			Die();
		}
		Debug.Log(gameObject.name + " has " + life + " health");
	}

	public void Die() {
		Debug.Log(gameObject.name + " is dead");
		
		Instantiate(explosion, transform.position, transform.rotation);
		
		GameObject.Find("/GameController").GetComponent<GameController>().GameOver();
		
		Destroy(gameObject);
	}

	}
}
