﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

public class TranslateGameScene : MonoBehaviour {

	private LanguageManager lm;
	// Use this for initialization
	void Start () {
		lm = LanguageManager.Instance;
		
		lm.ChangeLanguage (PlayerPrefs.GetString (Settings.KEY_LANGUEGE));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
}
