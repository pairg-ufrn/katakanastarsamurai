﻿namespace SmartLocalization
{
	using UnityEngine;
	using System.Collections.Generic;

public class TranslateSplashSceneScript : MonoBehaviour {

	private LanguageManager thisLanguageManager;
	private string code;

	// Use this for initialization
	void Start () {
			thisLanguageManager = LanguageManager.Instance;
			code = thisLanguageManager.CurrentlyLoadedCulture.languageCode;
			translate ();
	}
	
	private void translate(){

	}

	// Update is called once per frame
	void Update () {
			if(thisLanguageManager.CurrentlyLoadedCulture.languageCode != code){
				translate();
				code = thisLanguageManager.CurrentlyLoadedCulture.languageCode;
			}

	}
}
}//namespace SmartLocalization