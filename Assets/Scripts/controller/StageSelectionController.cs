﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class StageSelectionController : MonoBehaviour {

	private BinaryFormatter bf = new BinaryFormatter ();
	private FileStream file = null;
	private List<LevelData> levelList = null;

	private string difficulty;
	private string mode;

	public GameObject levelPrefb;
	
	private LevelData ld;


	// Use this for initialization
	void Start () {

		if (PlayerPrefs.GetInt("paused") == 1) {
			Time.timeScale = 1;
			PlayerPrefs.SetInt("paused",0);
		}

		if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) != null && (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_NORMAL || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD ) ) {
			difficulty = PlayerPrefs.GetString (Settings.KEY_DIFFICULTY);
		} else {
			difficulty = Settings.DIFFICULTY_NORMAL;
		}

		if (PlayerPrefs.GetString (Settings.KEY_MODE) != null && (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA || PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_ROMAJI)) {
			mode = PlayerPrefs.GetString (Settings.KEY_MODE);
		}
		else {
			mode = Settings.MODE_HIRAGANA;
		}

 

		FileStream file = null;
		
		Debug.Log(File.Exists (Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat"));    
		
		Debug.Log(Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat");    
		
		if (!File.Exists (Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat")) {

			Debug.Log("entrou para criar");    

			file = File.Create (Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat");
			
			levelList= new List<LevelData>();

			
			ld = new LevelData ();
			ld.index = 1;
			ld.state = LevelData.states.available;
			levelList.Add(ld);

			for (int i = 1; i < 25; i++) {
				ld = new LevelData ();
				ld.index = i+1;
				ld.state = LevelData.states.closed;
				levelList.Add(ld);
			}

			bf.Serialize (file,levelList);
			
		} else {
			Debug.Log("entrou para ler do arquivo");    

			file = File.Open(Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat", FileMode.Open);
			
			levelList = (List<LevelData>) bf.Deserialize(file);
		}
		
		file.Close ();

		float y1 = 0;
		float x1 = 0;

		foreach(LevelData ld in levelList) {
			

				switch(ld.index % 5){

					case 0:
						x1 = 10;
						
					break;

					case 1:
						x1 = -10;		
					break;

					case 2:
						x1 = -5;		
					break;

					case 3:
						x1 = 0;		
					break;

					case 4:
						x1 = 5;		
					break;

				}

				if(y1 > 14){
					y1 = 0;
				}


				var spawnPoint = new Vector2(x1, (6.5f-y1) + transform.position.y );

				levelPrefb.GetComponent<LoadLevelScript>().SetLevelData(ld);

				Instantiate(levelPrefb, spawnPoint, Quaternion.identity);

				if (ld.index % 5==0){
					y1+=3.2f;
				}

			
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	}
}