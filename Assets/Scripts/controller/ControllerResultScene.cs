﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class ControllerResultScene : MonoBehaviour {

	private BinaryFormatter bf = new BinaryFormatter ();
	private FileStream file = null;
	private List<LevelData> levelList = null;

	public GUIText tile;
	public Sprite failureStar;
	public GameObject Star;
	public GameObject buttonMenu;
	public GameObject buttonReplay;
	public GameObject buttonNext;
	private string result;
	private LevelData ld;
	private int level;

	void Start () {

		FileStream file = null;

		result = PlayerPrefs.GetString ("result");
		level =  PlayerPrefs.GetInt ("level");

		Debug.Log (level);

		buttonReplay.GetComponent<ResultButtonScript>().level = level;

		if (result == "failure") {
			tile.text = "Falhou";
			Star.GetComponent<SpriteRenderer>().sprite = failureStar;
			ResultButtonScript b =  buttonNext.GetComponent<ResultButtonScript>();
			b.disable();

		} else {
			buttonNext.GetComponent<ResultButtonScript>().level = level+1;	

			//if (File.Exists (Application.persistentDataPath + "/levelDate.dat")) {

				file = File.Open (Application.persistentDataPath + "/levelDate.dat", FileMode.Open);
				levelList = (List<LevelData>)bf.Deserialize (file);

				levelList[level].state = LevelData.states.available;

				if (result == "cleared"){
					levelList[level-1].state = LevelData.states.cleared;
				}
				else {
				//	levelList[level-1].state = LevelData.states.fully_cleared;
				}

				file.Close ();

				file = File.Create(Application.persistentDataPath + "/levelDate.dat");

				bf.Serialize (file,levelList);

				file.Close ();

				Debug.Log (level);
				
			//}
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
