﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModeSelectionController : MonoBehaviour {

	protected string difficulty;
	protected string mode;
	public GameObject modePrefab;
	public LoadModeScript seleted;
	
	protected GameObject[] optionsLoad;

	public GameObject ModeText;

		protected LanguageManager lm;


	// Use this for initialization
	void Start () {

		optionsLoad = new GameObject[3];

		lm = LanguageManager.Instance;
		
			/*if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) != null && (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_NORMAL || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD ) ) {
			difficulty = PlayerPrefs.GetString (Settings.KEY_DIFFICULTY);
		} else {
			difficulty = Settings.DIFFICULTY_NORMAL;
		}
		
		if (PlayerPrefs.GetString (Settings.KEY_MODE) != null && (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA || PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_ROMAJI)) {
			mode = PlayerPrefs.GetString (Settings.KEY_MODE);
		}
		else {
			mode = Settings.MODE_HIRAGANA;
		}*/

		mode = Settings.MODE_HIRAGANA;

		modePrefab.GetComponent<LoadModeScript> ().controller = this;		

		modePrefab.GetComponent<LoadModeScript>().mode =  Settings.MODE_HIRAGANA ;
		modePrefab.GetComponent<LoadModeScript>().difficulty =  Settings.DIFFICULTY_EASY ;

		Vector2 spawnPoint = new Vector2(transform.position.x - 11f ,  transform.position.y );
		
		optionsLoad[0] = Instantiate(modePrefab, spawnPoint, Quaternion.identity) as GameObject;

		modePrefab.GetComponent<LoadModeScript>().mode =  Settings.MODE_HIRAGANA ;
		modePrefab.GetComponent<LoadModeScript>().difficulty =  Settings.DIFFICULTY_NORMAL ;

		spawnPoint = new Vector2(transform.position.x, transform.position.y );

		optionsLoad[1] = Instantiate(modePrefab, spawnPoint, Quaternion.identity) as GameObject;

		modePrefab.GetComponent<LoadModeScript>().mode =  Settings.MODE_HIRAGANA ;
		modePrefab.GetComponent<LoadModeScript>().difficulty =  Settings.DIFFICULTY_HARD ;

		spawnPoint = new Vector2(transform.position.x +11f, transform.position.y  );
		
		optionsLoad[2] = Instantiate(modePrefab, spawnPoint, Quaternion.identity) as GameObject;


	}
	
	

	// Update is called once per frame
	void Update () {
	
			if (seleted != null) {

					string mode;

					if (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA) {
							mode = lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameHiragana");
					} else {
							mode = lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameRomaji");
					}

					if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY) {
							ModeText.GetComponent<GUIText> ().text =  "  " + lm.GetTextValue ("SplashScene.OptionsMenu.buttonDifficultyEasy");
					} else if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD) {
							ModeText.GetComponent<GUIText> ().text =  "  " + lm.GetTextValue ("SplashScene.OptionsMenu.buttonDifficultyHard");
					} else {
							ModeText.GetComponent<GUIText> ().text =  "  " + lm.GetTextValue ("SplashScene.OptionsMenu.buttonDifficultyNormal");
					}

	 
			} else {
				if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_NORMAL){
					//optionsLoad[1].GetComponent<LoadModeScript>().mode = PlayerPrefs.GetString (Settings.KEY_MODE);
					optionsLoad[1].GetComponent<LoadModeScript>().select();
				}
				if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY ){
					//optionsLoad[0].GetComponent<LoadModeScript>().mode = PlayerPrefs.GetString (Settings.KEY_MODE);
					optionsLoad[0].GetComponent<LoadModeScript>().select();
				}
				if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD ){
					//optionsLoad[2].GetComponent<LoadModeScript>().mode = PlayerPrefs.GetString (Settings.KEY_MODE);
					optionsLoad[2].GetComponent<LoadModeScript>().select();
				}




			}

	}
}
}