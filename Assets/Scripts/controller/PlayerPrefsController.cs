﻿namespace SmartLocalization
{

using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

	public class PlayerPrefsController : MonoBehaviour {

		private LanguageManager lm;
		private BinaryFormatter bf = new BinaryFormatter ();
		private FileStream file = null;
		private string fileName = "/Setting.dat";
		private Settings settings;



		void Start () {
			loadPlayerPrefs ();
		
		}

		void loadPlayerPrefs(){
			FileStream file = null;
			
			lm = LanguageManager.Instance;
			
			Debug.Log(File.Exists (Application.persistentDataPath + fileName));    
			
			Debug.Log(Application.persistentDataPath + fileName);    
			
			if (!File.Exists (Application.persistentDataPath + fileName)) {

				file = File.Create (Application.persistentDataPath + fileName);
				
				settings = new Settings();
				
				settings.currentDifficulty = Settings.DIFFICULTY_NORMAL;
				
				settings.currentMode = Settings.MODE_HIRAGANA;
				
				if (lm.CurrentlyLoadedCulture != null){
					settings.currentLanguege = lm.CurrentlyLoadedCulture.languageCode;
				}
				
				
				bf.Serialize (file,settings);
				
			} else {
				Debug.Log("entrou para ler do arquivo");    
				
				file = File.Open(Application.persistentDataPath + fileName, FileMode.Open);
				
				settings = (Settings) bf.Deserialize(file);
			}
			
			file.Close ();

			lm.ChangeLanguage(settings.currentLanguege);
			PlayerPrefs.SetString (Settings.KEY_DIFFICULTY, settings.currentDifficulty);
			PlayerPrefs.SetString (Settings.KEY_MODE, settings.currentMode);
			PlayerPrefs.SetString (Settings.KEY_LANGUEGE, settings.currentLanguege);
		}

		void savePlayerPrefs(){
			file = File.Open (Application.persistentDataPath + fileName, FileMode.Open);
			settings = (Settings)bf.Deserialize (file);
			
			settings.currentDifficulty = PlayerPrefs.GetString (Settings.KEY_DIFFICULTY);
			settings.currentMode = PlayerPrefs.GetString (Settings.KEY_MODE);
			settings.currentLanguege = PlayerPrefs.GetString (Settings.KEY_LANGUEGE);

			file.Close ();
			
			file = File.Create(Application.persistentDataPath + fileName);
			
			bf.Serialize (file,settings);
			
			file.Close ();
							
		
		}
		
		// Update is called once per frame
		void Update () {
		
			if(settings.currentDifficulty != PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) || settings.currentMode != PlayerPrefs.GetString (Settings.KEY_MODE) || settings.currentLanguege != PlayerPrefs.GetString (Settings.KEY_LANGUEGE)){
				savePlayerPrefs();
				loadPlayerPrefs();
			}

		}
	}
}