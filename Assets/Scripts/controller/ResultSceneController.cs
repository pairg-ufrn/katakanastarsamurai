﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class ResultSceneController : MonoBehaviour {

	private BinaryFormatter bf = new BinaryFormatter ();
	private FileStream file = null;
	private List<LevelData> levelList = null;

	public GUIText tile;
	public Sprite failureStar;
	public GameObject Star;
	public GameObject buttonMenu;
	public GameObject buttonReplay;
	public GameObject buttonNext;
	private string result;
	private LevelData ld;
	private int level;

	private string difficulty;
	private string mode;

	private LanguageManager lm;

	void Start () {

		FileStream file = null;

		lm = LanguageManager.Instance;
		result = PlayerPrefs.GetString (LevelData.KEY_RESULT);
		level =  PlayerPrefs.GetInt (LevelData.KEY_LEVEL);


		buttonReplay.GetComponent<ResultButtonScript>().level = level;

		if (result == LevelData.KEY_RESULT_FAIL) {
			tile.text = lm.GetTextValue("ResultScene.LabelResult.fail");
			Star.GetComponent<SpriteRenderer>().sprite = failureStar;
			ResultButtonScript b =  buttonNext.GetComponent<ResultButtonScript>();
			b.disable();

		}
		else if (result == LevelData.KEY_RESULT_SUCCESS || result == LevelData.KEY_RESULT_FULLY_SUCCESS) {
		
			if (level < 25){
				tile.text = lm.GetTextValue("ResultScene.LabelResult.success");


				buttonNext.GetComponent<ResultButtonScript>().level = level+1;	


				if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) != null && (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_NORMAL || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD ) ) {
					difficulty = PlayerPrefs.GetString (Settings.KEY_DIFFICULTY);
				} else {
					difficulty = Settings.DIFFICULTY_NORMAL;
				}
				
				if (PlayerPrefs.GetString (Settings.KEY_MODE) != null && (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA || PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_ROMAJI)) {
					mode = PlayerPrefs.GetString (Settings.KEY_MODE);
				}
				else {
					mode = Settings.MODE_HIRAGANA;
				}

				file = File.Open (Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat", FileMode.Open);
				levelList = (List<LevelData>)bf.Deserialize (file);

				levelList[level].state = LevelData.states.available;

				if (result == LevelData.KEY_RESULT_SUCCESS){
					levelList[level-1].state = LevelData.states.cleared;
				}
				else if (result == LevelData.KEY_RESULT_FULLY_SUCCESS){
	//				levelList[level-1].state = LevelData.states.fully_cleared;
				}

				file.Close ();

				file = File.Create(Application.persistentDataPath + "/LD."+difficulty+"."+mode+".dat");

				bf.Serialize (file,levelList);

				file.Close ();

			}
			else{
					tile.text = lm.GetTextValue("Game Over");
					ResultButtonScript b =  buttonNext.GetComponent<ResultButtonScript>();
					b.disable();
			}
			
			
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
}