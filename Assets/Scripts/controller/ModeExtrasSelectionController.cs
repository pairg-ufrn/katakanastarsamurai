﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModeExtrasSelectionController : ModeSelectionController {

	private string difficulty;
	private string mode;
	public GameObject modePrefab;
	public LoadModeScript seleted;
	
		private GameObject[] optionsLoad;

	public GameObject ModeText;

		private LanguageManager lm;


	// Use this for initialization
	void Start () {

		optionsLoad = new GameObject[2];

		lm = LanguageManager.Instance;
		
		if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) != null && (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_NORMAL || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY || PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD ) ) {
			difficulty = PlayerPrefs.GetString (Settings.KEY_DIFFICULTY);
		} else {
			difficulty = Settings.DIFFICULTY_NORMAL;
		}
		
		if (PlayerPrefs.GetString (Settings.KEY_MODE) != null && (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA || PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_ROMAJI)) {
			mode = PlayerPrefs.GetString (Settings.KEY_MODE);
		}
		else {
			mode = Settings.MODE_HIRAGANA;
		}

		modePrefab.GetComponent<LoadModeScript> ().controller = this;		

		modePrefab.GetComponent<LoadModeScript>().mode =  Settings.MODE_HIRAGANA ;
			modePrefab.GetComponent<LoadModeScript>().difficulty =  difficulty ;

		Vector2 spawnPoint = new Vector2(transform.position.x - 7f ,  transform.position.y );
		
		optionsLoad[0] = Instantiate(modePrefab, spawnPoint, Quaternion.identity) as GameObject;

		modePrefab.GetComponent<LoadModeScript>().mode =  Settings.MODE_ROMAJI ;
			modePrefab.GetComponent<LoadModeScript>().difficulty =  difficulty ;

		spawnPoint = new Vector2(transform.position.x +7, transform.position.y );

		optionsLoad[1] = Instantiate(modePrefab, spawnPoint, Quaternion.identity) as GameObject;




	}
	
	// Update is called once per frame
	void Update () {
	
			if (seleted != null) {

					string mode;

					if (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA) {
							mode = lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameHiragana");
					} else {
							mode = lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameRomaji");
					}



	 
			} else {
				if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_NORMAL){
					optionsLoad[1].GetComponent<LoadModeScript>().mode = PlayerPrefs.GetString (Settings.KEY_MODE);
					optionsLoad[1].GetComponent<LoadModeScript>().select();
				}
				if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY ){
					optionsLoad[0].GetComponent<LoadModeScript>().mode = PlayerPrefs.GetString (Settings.KEY_MODE);
					optionsLoad[0].GetComponent<LoadModeScript>().select();
				}





			}

	}
}
}