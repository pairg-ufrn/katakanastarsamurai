﻿namespace SmartLocalization
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;


	public class GameController : MonoBehaviour {

	

		public GUIText scoreText;
		public GUIText restartText;
		public GUIText gameOverText;
		public GUIText levelDisplayText;
		public GUIText enemyCountText;
		
		public GameObject bullet;
		
		RuntimePlatform platform  = Application.platform;
		
		private bool gameOver;
		private bool levelDisplay;
		private bool restart;
		private bool paused;

		private List<Syllable> syllableSelected;
				
		
		private GameObject buttonRegistred;
		private GameObject enemyRegistred;
		
		private int score;
		private int enemyCount;
		private int scoreLevel;
		private int enemyCountList;
		private LanguageManager lm;
		
		public LevelData level;


		// Use this for initialization
		void Start () {
		//	loadData();

			LevelFactory factory = new LevelFactory ();

			buttonRegistred = null;
			enemyRegistred = null;
			
			level = factory.getLevel(PlayerPrefs.GetString(Settings.KEY_DIFFICULTY),PlayerPrefs.GetString(Settings.KEY_MODE),PlayerPrefs.GetInt(LevelData.KEY_LEVEL));
			
			paused = false;
			gameOver = false;
			restart = false;
			restartText.text = "";
			gameOverText.text = "";
			levelDisplayText.text = lm.GetTextValue("General.Level")+" "+level;		

			
			score = level.highScore;
			enemyCount = level.enemyCount;
			
		//	selectedDictionaryLevel();
			
			UpdateScore ();

		}
		
		// Update is called once per frame
		void Update () {
			if (restart){			
				PlayerPrefs.SetString(LevelData.KEY_RESULT, LevelData.KEY_RESULT_FAIL);
				Application.LoadLevel ("ResultScene");  		
			}
			
			if (gameOver){
				restart = true;	
			}
			
			if (enemyCount == 0){
				PlayerPrefs.SetString(LevelData.KEY_RESULT, LevelData.KEY_RESULT_SUCCESS);
				Application.LoadLevel ("ResultScene");  
			}
		}

		
		
		public Syllable getSyllable(){
			
			return syllableSelected[Random.Range (0, syllableSelected.Count)];
		}
		
			
		
		public void AddScore (int newScoreValue){
			score += newScoreValue;
			enemyCount--;
			UpdateScore ();
			UpdateEnemyCount();
			
		}
		
		public void UpdateEnemyCount (){
			enemyCountText.text = "" + enemyCount;
		}
		
		public void registerButton(GameObject touchable){
			

			if (touchable.name == "button(Clone)"){
				
				if (buttonRegistred != null){
					buttonRegistred.GetComponent<ButtonGameBehaviour>().unblock();
					buttonRegistred.GetComponent<ButtonGameBehaviour>().unSeleted();
					
				}
				
				buttonRegistred = touchable;
				buttonRegistred.GetComponent<ButtonGameBehaviour>().seleted();
			}
			
			checkRegister();
			
		}
		
		public void checkRegister(){
			
			if (enemyRegistred != null && buttonRegistred!=null){
				if (enemyRegistred.GetComponent<EnemyBehaviour>().button == buttonRegistred){
					
					//shootBullet(); // para atriar a estrela 
					
					enemyRegistred.GetComponent<EnemyBehaviour>().die();
					Debug.Log("enemyRegistred.button and buttonRegistred  are same object");
					AddScore(1);
				}
				else if (enemyRegistred.GetComponent<EnemyBehaviour>().button.transform.FindChild("text").GetComponent<TextMesh>().text == buttonRegistred.transform.FindChild("text").GetComponent<TextMesh>().text){
					enemyRegistred.GetComponent<EnemyBehaviour>().die();
					Debug.Log("matou");
					AddScore(1);
					
					Debug.Log("enemyRegistred.button and buttonRegistred  has same text");
					
					buttonRegistred.GetComponent<ButtonGameBehaviour>().die();
					
				}
				
				enemyRegistred.GetComponent<EnemyBehaviour>().unSeleted();
				buttonRegistred.GetComponent<ButtonGameBehaviour>().unSeleted();
				
				buttonRegistred = null;
				enemyRegistred = null;
				
				
			}
			
		}
		
		public void shootBullet(){
			
			
			//bullet.rigidbody2D.y.x = Mathf.Sign(enemyRegistred.transform.position.x - transform.position.x)*100000 ;
			
			// 	rigidbody2D.angularVelocity =  Mathf.MoveTowardsAngle(enemyRegistred.position.x - transform.position.x,enemyRegistred.position.y - transform.position.y, enemyRegistred.position.z - transform.position.z );
			
			// chase star math is rocks  
			
			Instantiate(bullet, transform.position, Quaternion.identity);
			
		}
		
		public void registerEnemy(GameObject touchable){
			

			if (touchable.name == "enemy(Clone)"){
				enemyRegistred = touchable;
				enemyRegistred.GetComponent<EnemyBehaviour>().seleted();
				
			}
			
			checkRegister();
			
		}
		
		public void UpdateScore ()
		{
			scoreText.text = "" + score;
		}
		
		public void restartGame ()
		{
			restart = true;
		}
		
		public void GameOver ()
		{
			gameOverText.text = "Game Over!";
			gameOver = true;
		}
		


	}
}