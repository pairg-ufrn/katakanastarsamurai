﻿#pragma strict

public var scoreText :GUIText;
public var restartText: GUIText ;
public var gameOverText: GUIText;
public var levelDisplayText: GUIText;
public var enemyCountText: GUIText;

public var bullet : GameObject;

var platform : RuntimePlatform = Application.platform;

private var gameOver: boolean;
private var levelDisplay: boolean;
private var restart : boolean;
private var paused : boolean;

private var Katakana: String[];
private var Hiragana: String[];
private var Romaji: String[];

private var KatakanaL1: String[];
private var HiraganaL1: String[];
private var RomajiL1: String[];

private var KatakanaL2 :String[];
private var HiraganaL2 :String[];
private var RomajiL2 :String[];

private var KatakanaL3 :String[];
private var HiraganaL3 :String[];
private var RomajiL3 :String[];

private var KatakanaL4 :String[];
private var HiraganaL4 :String[];
private var RomajiL4 :String[];

private var KatakanaL5 :String[];
private var HiraganaL5 :String[];
private var RomajiL5 :String[];

private var KatakanaL6 :String[];
private var HiraganaL6 :String[];
private var RomajiL6 :String[];

private var KatakanaL7 :String[];
private var HiraganaL7 :String[];
private var RomajiL7 :String[];

private var KatakanaL8 :String[];
private var HiraganaL8 :String[];
private var RomajiL8 :String[];

private var KatakanaL9 :String[];
private var HiraganaL9 :String[];
private var RomajiL9 :String[];

private var KatakanaL10 :String[];
private var HiraganaL10 :String[];
private var RomajiL10 :String[];

private var KatakanaL11 :String[];
private var HiraganaL11 :String[];
private var RomajiL11 :String[];

private var KatakanaL12 :String[];
private var HiraganaL12 :String[];
private var RomajiL12 :String[];

private var KatakanaL13 :String[];
private var HiraganaL13 :String[];
private var RomajiL13 :String[];

private var KatakanaL14 :String[];
private var HiraganaL14 :String[];
private var RomajiL14 :String[];

private var KatakanaL15 :String[];
private var HiraganaL15 :String[];
private var RomajiL15 :String[];

private var KatakanaL16 :String[];
private var HiraganaL16 :String[];
private var RomajiL16 :String[];

private var KatakanaL17 :String[];
private var HiraganaL17 :String[];
private var RomajiL17 :String[];

private var KatakanaL18 :String[];
private var HiraganaL18 :String[];
private var RomajiL18 :String[];

private var KatakanaL19 :String[];
private var HiraganaL19 :String[];
private var RomajiL19 :String[];

private var KatakanaL20 :String[];
private var HiraganaL20 :String[];
private var RomajiL20 :String[];

private var KatakanaL21 :String[];
private var HiraganaL21 :String[];
private var RomajiL21 :String[];

private var KatakanaL22 :String[];
private var HiraganaL22 :String[];
private var RomajiL22 :String[];

private var KatakanaSelected :String[];
private var HiraganaSelected :String[];
private var RomajiSelected :String[];

private var buttonRegistred: GameObject;
private var buttonsRegistred: GameObject[];
private var enemyRegistred: GameObject;

private var score: int;
private var enemyCount: int;
private var scoreLevel :int[];
private var enemyCountList: int[];

private var levelToArray : int;

private var lastIndex: int = -1; 

private var lastbutone : int = -1;

public static var KEY_LEVEL  : String = "level" ;
public static var KEY_RESULT_SUCCESS  : String = "success" ;
public static var KEY_RESULT_FULLY_SUCCESS  : String = "fully_success" ;
public static var KEY_RESULT_FAIL  : String = "fail" ;
public static var KEY_RESULT  : String = "result" ;

public static  var KEY_DIFFICULTY : String  = "DIFFICULTY";
public static  var KEY_MODE : String  = "MODE";
public static  var KEY_LANGUEGE : String  = "LANGUEGE";

public static  var DIFFICULTY_EASY : String = "easy";
public static  var DIFFICULTY_NORMAL : String = "normal";
public static  var DIFFICULTY_HARD : String = "hard";
public static  var MODE_HIRAGANA : String = "hiragana";
public static  var MODE_ROMAJI : String = "romaji";

public var level: int ;

function Start () {
				
		loadData();

		buttonRegistred = null;
		enemyRegistred = null;

		buttonsRegistred = new Array (5);

		level = PlayerPrefs.GetInt(KEY_LEVEL);
	
		levelToArray = level -1;

		paused = false;
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";

		score = scoreLevel[levelToArray];
		enemyCount = enemyCountList[levelToArray];
		
		selectedDictionaryLevel();
		
		UpdateEnemyCount();
		
		UpdateScore ();

/*		var i : int;
		for ( i = 0; i < KatakanaSelected.Length; i++ )
			Debug.Log(KatakanaSelected[i]);			
*/
		
//		levelDisplayText.text = levelDisplayText.text+" "+level;
								
}


function getIndexCharacters(){
	
	var i: int;
	
	if(level < 1){
		if (PlayerPrefs.GetString(KEY_DIFFICULTY) == DIFFICULTY_EASY){
			
			
			if (lastIndex < KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length && lastIndex >-1){
				
				i = Random.Range (KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length, KatakanaSelected.Length);
			}
			else{
				i = Random.Range (0, KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length);
			}
			
			lastIndex = i;
			
			return i;
		
		}
		else if (PlayerPrefs.GetString(KEY_DIFFICULTY) == DIFFICULTY_NORMAL){

			if (lastIndex < KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length && lastbutone < KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length && lastIndex > -1 && lastbutone > -1){		
				i = Random.Range (KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length, KatakanaSelected.Length);
			}
			else{
				i = Random.Range (0, KatakanaSelected.Length - getKatakanaDictionaryLevel(level).Length);
			}
			
			lastbutone = lastIndex;
			
			lastIndex = i;
			
			return i;
			
		}
	}			
	return Random.Range (0, KatakanaSelected.Length);
}

function getKatakanaIndex(index:int){

	return KatakanaSelected[index];
}


function getHiraganaIndex(index:int){
	
	return HiraganaSelected[index];

}

function getRomajiIndex(index:int){
	
	return RomajiSelected[index];

}



function Update () {
	
	levelDisplayText.text = levelDisplayText.text+" "+level;		
	
	if (restart){			
		PlayerPrefs.SetString(KEY_RESULT, KEY_RESULT_FAIL);
	    Application.LoadLevel ("ResultScene");  		
	}
	
	if (gameOver){
		restart = true;	
	}
	
	if (enemyCount == 0){
		PlayerPrefs.SetString(KEY_RESULT, KEY_RESULT_SUCCESS);
		Application.LoadLevel ("ResultScene");  
	}
	
	
}

function AddScore (newScoreValue:int){
	score += newScoreValue;
	enemyCount--;
	UpdateScore ();
	UpdateEnemyCount();
	
}

function UpdateEnemyCount (){
	enemyCountText.text = "" + enemyCount;
}

function registerButton(touchable: GameObject){
	
	if (level < 22){
		if (touchable.name == "ButtonGame(Clone)"){
		
			if (buttonRegistred != null){
				buttonRegistred.GetComponent(ButtonBehaviourScript).unblock();
				buttonRegistred.GetComponent(ButtonBehaviourScript).unSeleted();

			}
			
			buttonRegistred = touchable;
			buttonRegistred.GetComponent(ButtonBehaviourScript).seleted();
		}

	}
	else {
	
		if (touchable.name == "ButtonGame(Clone)"){
		
			var i : int = 0;
			
			var full : boolean = true; 
			
			
			for(i = 0; i < 5; i++){
				if (buttonsRegistred[i] == null){
					buttonsRegistred[i] = touchable;
					buttonsRegistred[i].GetComponent(ButtonBehaviourScript).seleted();
					full = false;
				}
			
			}
			if(full){
				
				
				buttonsRegistred[0].GetComponent(ButtonBehaviourScript).unblock();
				buttonsRegistred[0].GetComponent(ButtonBehaviourScript).unSeleted();

				buttonsRegistred[0] = buttonsRegistred[1];
				buttonsRegistred[1] = buttonsRegistred[2];
				buttonsRegistred[2] = buttonsRegistred[3];
				buttonsRegistred[3] = buttonsRegistred[4];
				buttonsRegistred[4] = touchable;
				
				buttonsRegistred[4].GetComponent(ButtonBehaviourScript).seleted();
				
			}
			
		}
			
	}

	
	

	checkRegister();
		
}

function checkRegister(){

	if (level < 22){
		if (enemyRegistred != null && buttonRegistred!=null){
			if (enemyRegistred.GetComponent(EnemyBehaviourScript).button == buttonRegistred){
				
				//shootBullet(); // para atriar a estrela 
				
				enemyRegistred.GetComponent(EnemyBehaviourScript).die();
				Debug.Log("enemyRegistred.button and buttonRegistred  are same object");
				AddScore(1);
			}
			else if (enemyRegistred.GetComponent(EnemyBehaviourScript).button.transform.FindChild("text").GetComponent(TextMesh).text == buttonRegistred.transform.FindChild("text").GetComponent(TextMesh).text){
				enemyRegistred.GetComponent(EnemyBehaviourScript).die();
				Debug.Log("matou");
				AddScore(1);
				
				Debug.Log("enemyRegistred.button and buttonRegistred  has same text");
				
				buttonRegistred.GetComponent(ButtonBehaviourScript).die();
				
			}
			
			enemyRegistred.GetComponent(EnemyBehaviourScript).unSeleted();
			buttonRegistred.GetComponent(ButtonBehaviourScript).unSeleted();
			
			buttonRegistred = null;
			enemyRegistred = null;
			 
		
		}
	}
	else{
	
				
		if (enemyRegistred != null){
			
			var listB : GameObject[] = enemyRegistred.GetComponent(BigEnemyBehaviourScript).button;
			var testCont : int = 0;
			
					
				
			for (var b : GameObject in listB){
				for (var br : GameObject in buttonsRegistred ){
					if (b == br || b.transform.FindChild("text").GetComponent(TextMesh).text == br.transform.FindChild("text").GetComponent(TextMesh).text){
						testCont = 5;	
					}
				}	
			}
			
		
			if (testCont == 5){
				
				enemyRegistred.GetComponent(BigEnemyBehaviourScript).die();
				Debug.Log("matou");
				AddScore(1);
				
				Debug.Log("enemyRegistred.button and buttonRegistred  has same text");
				
				for (var br : GameObject in buttonsRegistred ){
					br.GetComponent(ButtonBehaviourScript).die();
				}	
			
				
			}
			else{
				enemyRegistred.GetComponent(BigEnemyBehaviourScript).unSeleted();
				//buttonsRegistred.GetComponent(ButtonBehaviourScript).unSeleted();
			
				for (var br : GameObject in buttonsRegistred ){
					br.GetComponent(ButtonBehaviourScript).unSeleted();
				}
			
				buttonRegistred = null;
				enemyRegistred = null;
			}
		
		}
	
	
	
	}

}



function registerEnemy(touchable: GameObject){
	
	Debug.Log("registrou um enemy");
	Debug.Log(touchable.name);
	if (touchable.name == "enemy(Clone)"){
		enemyRegistred = touchable;
		enemyRegistred.GetComponent(EnemyBehaviourScript).seleted();
		
	}
	else if (touchable.name == "BigEnemy(Clone)"){
		enemyRegistred = touchable;
		enemyRegistred.GetComponent(BigEnemyBehaviourScript).seleted();
	}
	
	checkRegister();

}
	
function UpdateScore ()
{
	scoreText.text = "" + score;
}

function restartGame ()
{
	restart = true;
}

function GameOver ()
{
	gameOver = true;
}

function concat(x : String[], y :String[] ){
	var size : int = x.Length + y.Length;
	var list : String[];
	list = Array(size);
	var z : int = 0;
	
	for ( z = 0; z < size-y.Length;z ++ )
		list[z]=x[z];


	for ( z = x.Length ; z < size ;z ++ )
		list[z]=y[z-x.Length];
		
	
	return list;
	
}



function loadData(){

	scoreLevel = new Array(25);
	enemyCountList = new Array(25);	

	enemyCountList[0] = 10;
	enemyCountList[1] = 10;  
	enemyCountList[2] = 12;  
	enemyCountList[3] = 12;  
	enemyCountList[4] = 12;  
	enemyCountList[5] = 15;  
	enemyCountList[6] = 15;  
	enemyCountList[7] = 15; 
	enemyCountList[8] = 18;  
	enemyCountList[9] = 18; 
	enemyCountList[10] = 18;  
	enemyCountList[11] = 21;  
	enemyCountList[12] = 21;  
	enemyCountList[13] = 21;  
	enemyCountList[14] = 24;  
	enemyCountList[15] = 24; 
	enemyCountList[16] = 24;  
	enemyCountList[17] = 27;  
	enemyCountList[18] = 27;  
	enemyCountList[19] = 27;  
	enemyCountList[20] = 30;  
	enemyCountList[21] = 30; 
	enemyCountList[22] = 5;  
	enemyCountList[23] = 10;  
	enemyCountList[24] = 15;
	
	scoreLevel[0] = 0;
	
	var x: int;
	var y: int;
	
	for(x = 1; x < 25; x++){
		for (y = 0; y < x; y ++ ){
			scoreLevel[x] += enemyCountList[y];
		}	
	}

	KatakanaL1 = new Array(5);
	HiraganaL1 = new Array(5);
	RomajiL1 = new Array(5);

	KatakanaL2 = new Array(5);
	HiraganaL2 = new Array(5);
	RomajiL2 = new Array(5);
	
	KatakanaL3 = new Array(5);
	HiraganaL3 = new Array(5);
	RomajiL3 = new Array(5);
		
	KatakanaL4 = new Array(5);
	HiraganaL4 = new Array(5);
	RomajiL4 = new Array(5);
	
	KatakanaL5 = new Array(5);
	HiraganaL5 = new Array(5);
	RomajiL5 = new Array(5);

	KatakanaL6 = new Array(5);
	HiraganaL6 = new Array(5);
	RomajiL6 = new Array(5);

	KatakanaL7 = new Array(5);
	HiraganaL7 = new Array(5);
	RomajiL7 = new Array(5);

	KatakanaL8 = new Array(3);
	HiraganaL8 = new Array(3);
	RomajiL8 = new Array(3);

	KatakanaL9 = new Array(5);
	HiraganaL9 = new Array(5);
	RomajiL9 = new Array(5);

	KatakanaL10 = new Array(3);
	HiraganaL10 = new Array(3);
	RomajiL10 = new Array(3);

	KatakanaL11 = new Array(5);
	HiraganaL11 = new Array(5);
	RomajiL11 = new Array(5);

	KatakanaL11 = new Array(5);
	HiraganaL11 = new Array(5);
	RomajiL11 = new Array(5);

	KatakanaL12 = new Array(5);
	HiraganaL12 = new Array(5);
	RomajiL12 = new Array(5);

	KatakanaL13 = new Array(5);
	HiraganaL13 = new Array(5);
	RomajiL13 = new Array(5);

	KatakanaL14 = new Array(5);
	HiraganaL14 = new Array(5);
	RomajiL14 = new Array(5);

	KatakanaL15 = new Array(5);
	HiraganaL15 = new Array(5);
	RomajiL15 = new Array(5);

	KatakanaL16 = new Array(4);
	HiraganaL16 = new Array(4);
	RomajiL16 = new Array(4);

	KatakanaL17 = new Array(5);
	HiraganaL17 = new Array(5);
	RomajiL17 = new Array(5);

	KatakanaL18 = new Array(6);
	HiraganaL18 = new Array(6);
	RomajiL18 = new Array(6);

	KatakanaL19 = new Array(6);
	HiraganaL19 = new Array(6);
	RomajiL19 = new Array(6);

	KatakanaL20 = new Array(6);
	HiraganaL20 = new Array(6);
	RomajiL20 = new Array(6);

	KatakanaL21 = new Array(6);
	HiraganaL21 = new Array(6);
	RomajiL21 = new Array(6);

	KatakanaL22 = new Array(3);
	HiraganaL22 = new Array(3);
	RomajiL22 = new Array(3);
	
	
	KatakanaL1 [0] = ("ア");
	KatakanaL1 [1] = ("イ");
	KatakanaL1 [2] = ("ウ");
	KatakanaL1 [3] = ("エ");
	KatakanaL1 [4] = ("オ");
	
	KatakanaL2 [0] = ("カ");
	KatakanaL2 [1] = ("キ");
	KatakanaL2 [2] = ("ク");
	KatakanaL2 [3] = ("ケ");
	KatakanaL2 [4] = ("コ");
	
	KatakanaL3 [0] = ("サ");
	KatakanaL3 [1] = ("シ");
	KatakanaL3 [2] = ("ス");
	KatakanaL3 [3] = ("セ");
	KatakanaL3 [4] = ("ソ");
	
	KatakanaL4 [0] = ("タ");
	KatakanaL4 [1] = ("チ");
	KatakanaL4 [2] = ("ツ");
	KatakanaL4 [3] = ("テ");
	KatakanaL4 [4] = ("ト");
	
	KatakanaL5 [0] = ("ナ");
	KatakanaL5 [1] = ("ニ");
	KatakanaL5 [2] = ("ヌ");
	KatakanaL5 [3] = ("ネ");
	KatakanaL5 [4] = ("ノ");
	
	KatakanaL6 [0] = ("ハ");
	KatakanaL6 [1] = ("ヒ");
	KatakanaL6 [2] = ("フ");
	KatakanaL6 [3] = ("ヘ");
	KatakanaL6 [4] = ("ホ");
	
	KatakanaL7 [0] = ("マ");
	KatakanaL7 [1] = ("ミ");
	KatakanaL7 [2] = ("ム");
	KatakanaL7 [3] = ("メ");
	KatakanaL7 [4] = ("モ");
	
	KatakanaL8 [0] = ("ヤ");
	KatakanaL8 [1] = ("ユ");
	KatakanaL8 [2] = ("ヨ");
	
	KatakanaL9 [0] = ("ラ");
	KatakanaL9 [1] = ("リ");
	KatakanaL9 [2] = ("ル");
	KatakanaL9 [3] = ("レ");
	KatakanaL9 [4] = ("ロ");
	
	KatakanaL10 [0] = ("ワ");
	KatakanaL10 [1] = ("ヲ");
	KatakanaL10 [2] = ("ン");
	
	KatakanaL11 [0] = ("ガ");
	KatakanaL11 [1] = ("ギ");
	KatakanaL11 [2] = ("グ");
	KatakanaL11 [3] = ("ゲ");
	KatakanaL11 [4] = ("ゴ");
	
	KatakanaL12 [0] = ("ザ");
	KatakanaL12 [1] = ("ジ");
	KatakanaL12 [2] = ("ズ");
	KatakanaL12 [3] = ("ゼ");
	KatakanaL12 [4] = ("ゾ");
	
	KatakanaL13 [0] = ("ダ");
	KatakanaL13 [1] = ("ヂ");
	KatakanaL13 [2] = ("ヅ");
	KatakanaL13 [3] = ("デ");
	KatakanaL13 [4] = ("ド");
	
	KatakanaL14 [0] = ("バ");
	KatakanaL14 [1] = ("ビ");
	KatakanaL14 [2] = ("ブ");
	KatakanaL14 [3] = ("ベ");
	KatakanaL14 [4] = ("ボ");
	
	KatakanaL15 [0] = ("パ");
	KatakanaL15 [1] = ("ピ");
	KatakanaL15 [2] = ("プ");
	KatakanaL15 [3] = ("ペ");
	KatakanaL15 [4] = ("ポ");
	
	KatakanaL16 [0] = ("ファ");
	KatakanaL16 [1] = ("フィ");
	KatakanaL16 [2] = ("フェ");
	KatakanaL16 [3] = ("フォ");
	
	KatakanaL17 [0] = ("ヴァ");
	KatakanaL17 [1] = ("ヴィ");
	KatakanaL17 [2] = ("ヴ");
	KatakanaL17 [3] = ("ヴェ");
	KatakanaL17 [4] = ("ヴォ");
	
	KatakanaL18 [0] = ("キャ");
	KatakanaL18 [1] = ("キュ");
	KatakanaL18 [2] = ("キョ");
	KatakanaL18 [3] = ("ギャ");
	KatakanaL18 [4] = ("ギュ");
	KatakanaL18 [5] = ("ギョ");
	
	KatakanaL19 [0] = ("シャ");
	KatakanaL19 [1] = ("シュ");
	KatakanaL19 [2] = ("ショ");
	KatakanaL19 [3] = ("ジャ");
	KatakanaL19 [4] = ("ジュ");
	KatakanaL19 [5] = ("ジョ");
	
	KatakanaL20 [0] = ("チャ");
	KatakanaL20 [1] = ("チュ");
	KatakanaL20 [2] = ("チョ");
	KatakanaL20 [3] = ("ヂャ");
	KatakanaL20 [4] = ("ヂュ");
	KatakanaL20 [5] = ("ヂョ");
	
	KatakanaL21 [0] = ("ニャ");
	KatakanaL21 [1] = ("ニュ");
	KatakanaL21 [2] = ("ニョ");
	KatakanaL21 [3] = ("ミャ");
	KatakanaL21 [4] = ("ミュ");
	KatakanaL21 [5] = ("ミョ");
	
	KatakanaL22 [0] = ("リャ");
	KatakanaL22 [1] = ("リュ");
	KatakanaL22 [2] = ("リョ");

	HiraganaL1[0] = ("あ");
	HiraganaL1[1] = ("い");
	HiraganaL1[2] = ("う");
	HiraganaL1[3] = ("え");
	HiraganaL1[4] = ("お");
	
	HiraganaL2[0] = ("か");
	HiraganaL2[1] = ("き");
	HiraganaL2[2] = ("く");
	HiraganaL2[3] = ("け");
	HiraganaL2[4] = ("こ");
	
	HiraganaL3[0] = ("さ");
	HiraganaL3[1] = ("し");
	HiraganaL3[2] = ("す");
	HiraganaL3[3] = ("せ");
	HiraganaL3[4] = ("そ");
	
	HiraganaL4[0] = ("た");
	HiraganaL4[1] = ("ち");
	HiraganaL4[2] = ("つ");
	HiraganaL4[3] = ("て");
	HiraganaL4[4] = ("と");
	
	HiraganaL5[0] = ("な");
	HiraganaL5[1] = ("に");
	HiraganaL5[2] = ("ぬ");
	HiraganaL5[3] = ("ね");
	HiraganaL5[4] = ("の");
	
	HiraganaL6[0] = ("は");
	HiraganaL6[1] = ("ひ");
	HiraganaL6[2] = ("ふ");
	HiraganaL6[3] = ("へ");
	HiraganaL6[4] = ("ほ");
	
	HiraganaL7[0] = ("ま");
	HiraganaL7[1] = ("み");
	HiraganaL7[2] = ("む");
	HiraganaL7[3] = ("め");
	HiraganaL7[4] = ("も");
	
	HiraganaL8[0] = ("や");
	HiraganaL8[1] = ("ゆ");
	HiraganaL8[2] = ("よ");
	
	HiraganaL9[0] = ("ら");
	HiraganaL9[1] = ("り");
	HiraganaL9[2] = ("る");
	HiraganaL9[3] = ("れ");
	HiraganaL9[4] = ("ろ");
	
	HiraganaL10[0] = ("わ");
	HiraganaL10[1] = ("を");
	HiraganaL10[2] = ("ん");
	
	HiraganaL11[0] = ("が");
	HiraganaL11[1] = ("ぎ");
	HiraganaL11[2] = ("ぐ");
	HiraganaL11[3] = ("げ");
	HiraganaL11[4] = ("ご");
	
	HiraganaL12[0] = ("ざ");
	HiraganaL12[1] = ("じ");
	HiraganaL12[2] = ("ず");
	HiraganaL12[3] = ("ぜ");
	HiraganaL12[4] = ("ぞ");
	
	HiraganaL13[0] = ("だ");
	HiraganaL13[1] = ("ぢ");
	HiraganaL13[2] = ("づ");
	HiraganaL13[3] = ("で");
	HiraganaL13[4] = ("ど");
	
	HiraganaL14[0] = ("ば");
	HiraganaL14[1] = ("び");
	HiraganaL14[2] = ("ぶ");
	HiraganaL14[3] = ("べ");
	HiraganaL14[4] = ("ぼ");
	
	HiraganaL15[0] = ("ぱ");
	HiraganaL15[1] = ("ぴ");
	HiraganaL15[2] = ("ぷ");
	HiraganaL15[3] = ("ぺ");
	HiraganaL15[4] = ("ぽ");
	
	/*HiraganaL16[0] = ("-");
	HiraganaL16[1] = ("-");
	HiraganaL16[2] = ("-");
	HiraganaL16[3] = ("-");
	
	HiraganaL17[0] = ("-");
	HiraganaL17[1] = ("-");
	HiraganaL17[2] = ("-");
	HiraganaL17[3] = ("-");
	HiraganaL17[4] = ("-");*/
	
	HiraganaL6[0] = ("ha");
	HiraganaL6[1] = ("hi");
	HiraganaL6[2] = ("fu");
	HiraganaL6[3] = ("he");
	HiraganaL6[4] = ("ho");

	HiraganaL7[0] = ("ma");
	HiraganaL7[1] = ("mi");
	HiraganaL7[2] = ("mu");
	HiraganaL7[3] = ("me");
	HiraganaL7[4] = ("mo");
	
	HiraganaL18[0] = ("きゃ");
	HiraganaL18[1] = ("きゅ");
	HiraganaL18[2] = ("きょ");
	HiraganaL18[3] = ("ぎゃ");
	HiraganaL18[4] = ("ぎゅ");
	HiraganaL18[5] = ("ぎょ");
	
	HiraganaL19[0] = ("しゃ");
	HiraganaL19[1] = ("しゅ");
	HiraganaL19[2] = ("しょ");
	HiraganaL19[3] = ("じゃ");
	HiraganaL19[4] = ("じゅ");
	HiraganaL19[5] = ("じょ");
	
	HiraganaL20[0] = ("ちゃ");
	HiraganaL20[1] = ("ちゅ");
	HiraganaL20[2] = ("ちゅ");
	HiraganaL20[3] = ("ぢゃ");
	HiraganaL20[4] = ("ぢゅ");
	HiraganaL20[5] = ("ぢょ");
	
	HiraganaL21[0] = ("にゃ");
	HiraganaL21[1] = ("にゅ");
	HiraganaL21[2] = ("にょ");
	HiraganaL21[3] = ("みゃ");
	HiraganaL21[4] = ("みゅ");
	HiraganaL21[5] = ("みょ");
	
	HiraganaL22[0] = ("りゃ");
	HiraganaL22[1] = ("りゅ");
	HiraganaL22[2] = ("りゅ");
	

	RomajiL1[0] = ("a");
	RomajiL1[1] = ("i");
	RomajiL1[2] = ("u");
	RomajiL1[3] = ("e");
	RomajiL1[4] = ("o");
	
	RomajiL2[0] = ("ka");
	RomajiL2[1] = ("ki");
	RomajiL2[2] = ("ku");
	RomajiL2[3] = ("ke");
	RomajiL2[4] = ("ko");
	
	RomajiL3[0] = ("sa");
	RomajiL3[1] = ("shi");
	RomajiL3[2] = ("su");
	RomajiL3[3] = ("se");
	RomajiL3[4] = ("so");
	
	RomajiL4[0] = ("ta");
	RomajiL4[1] = ("chi");
	RomajiL4[2] = ("tsu");
	RomajiL4[3] = ("te");
	RomajiL4[4] = ("to");
	
	RomajiL5[0] = ("na");
	RomajiL5[1] = ("ni");
	RomajiL5[2] = ("nu");
	RomajiL5[3] = ("ne");
	RomajiL5[4] = ("no");
	
	RomajiL6[0] = ("ha");
	RomajiL6[1] = ("hi");
	RomajiL6[2] = ("fu");
	RomajiL6[3] = ("he");
	RomajiL6[4] = ("ho");
	
	RomajiL7[0] = ("ma");
	RomajiL7[1] = ("mi");
	RomajiL7[2] = ("mu");
	RomajiL7[3] = ("me");
	RomajiL7[4] = ("mo");
	
	RomajiL8[0] = ("ya");
	RomajiL8[1] = ("yu");
	RomajiL8[2] = ("yo");
	
	RomajiL9[0] = ("ra");
	RomajiL9[1] = ("ri");
	RomajiL9[2] = ("ru");
	RomajiL9[3] = ("re");
	RomajiL9[4] = ("ro");
	
	RomajiL10[0] = ("wa");
	RomajiL10[1] = ("wo");
	RomajiL10[2] = ("n");
	
	RomajiL11[0] = ("ga");
	RomajiL11[1] = ("gi");
	RomajiL11[2] = ("gu");
	RomajiL11[3] = ("ge");
	RomajiL11[4] = ("go");
	
	RomajiL12[0] = ("za");
	RomajiL12[1] = ("ji");
	RomajiL12[2] = ("zu");
	RomajiL12[3] = ("ze");
	RomajiL12[4] = ("zo");
	
	RomajiL13[0] = ("da");
	RomajiL13[1] = ("dji");
	RomajiL13[2] = ("dzu");
	RomajiL13[3] = ("de");
	RomajiL13[4] = ("do");
	
	RomajiL14[0] = ("ba");
	RomajiL14[1] = ("bi");
	RomajiL14[2] = ("bu");
	RomajiL14[3] = ("be");
	RomajiL14[4] = ("bo");
	
	RomajiL15[0] = ("pa");
	RomajiL15[1] = ("pi");
	RomajiL15[2] = ("pu");
	RomajiL15[3] = ("pe");
	RomajiL15[4] = ("po");
	
	RomajiL16[0] = ("fa");
	RomajiL16[1] = ("fi");
	RomajiL16[2] = ("fe");
	RomajiL16[3] = ("fo");
	
	RomajiL17[0] = ("va");
	RomajiL17[1] = ("vi");
	RomajiL17[2] = ("vu");
	RomajiL17[3] = ("ve");
	RomajiL17[4] = ("vo");
	
	RomajiL18[0] = ("kya");
	RomajiL18[1] = ("kyu");
	RomajiL18[2] = ("kyo");
	RomajiL18[3] = ("gya");
	RomajiL18[4] = ("gyu");
	RomajiL18[5] = ("gyo");
	
	RomajiL19[0] = ("sha");
	RomajiL19[1] = ("shu");
	RomajiL19[2] = ("sho");
	RomajiL19[3] = ("ja");
	RomajiL19[4] = ("ju");
	RomajiL19[5] = ("jo");
	
	RomajiL20[0] = ("cha");
	RomajiL20[1] = ("chu");
	RomajiL20[2] = ("cho");
	RomajiL20[3] = ("dya");
	RomajiL20[4] = ("dyu");
	RomajiL20[5] = ("dyo");
	
	RomajiL21[0] = ("nya");
	RomajiL21[1] = ("nyu");
	RomajiL21[2] = ("nyo");
	RomajiL21[3] = ("mya");
	RomajiL21[4] = ("myu");
	RomajiL21[5] = ("myo");
	
	RomajiL22[0] = ("rya");
	RomajiL22[1] = ("ryu");
	RomajiL22[2] = ("ryo");

}


function selectedDictionaryLevel(){
		
	switch (level){

		case 1 :
			
			KatakanaSelected = KatakanaL1;
			HiraganaSelected = HiraganaL1;
			RomajiSelected = RomajiL1;
							
		break;

		case 2 :
			
			KatakanaSelected = concat(KatakanaL2, KatakanaL1);
			HiraganaSelected = concat(HiraganaL2, HiraganaL1);
			RomajiSelected = concat(RomajiL2, RomajiL1);
							
		break;

		case 3:
			
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , KatakanaL3 ));
			HiraganaSelected = concat( HiraganaL1, concat(HiraganaL2, HiraganaL3));
			RomajiSelected = concat( RomajiL1, concat(RomajiL2, RomajiL3));
					
		break;
		
		case 4:
			
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , KatakanaL4)));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , HiraganaL4)));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , RomajiL4)));
			
		break;
		
		case 5:
			
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4, KatakanaL5 ))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, HiraganaL5 ))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, RomajiL5))));

					
		break;
		
		case 6:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 ,  KatakanaL6 )))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , HiraganaL6)))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, RomajiL6)))));

					
		break;
		
		case 7:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , KatakanaL7))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 , HiraganaL7))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, RomajiL7 ))))));
			
		break;
		
		case 8:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 ,  KatakanaL8)))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,  HiraganaL8)))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 , RomajiL8 )))))));
					
									
		break;
		
		case 9:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8,  KatakanaL9))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8, HiraganaL9   ))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,RomajiL9 ))))))));

							
		break;
		
		case 10:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , KatakanaL10)))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   ,HiraganaL10)))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 , RomajiL10  )))))))));

							
		break;

		case 11:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10,KatakanaL11))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10,  HiraganaL11 ))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  , RomajiL11 ))))))))));
							
		break;
		
		case 12:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, KatakanaL12  )))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,HiraganaL12  )))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 , RomajiL12 )))))))))));
							
		break;
		
		case 13:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  KatakanaL13  ))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,  HiraganaL13   ))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 , RomajiL13 ))))))))))));
							
		break;
		
		case 14:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 ,  KatakanaL14 )))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   , HiraganaL14  )))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 , RomajiL14 )))))))))))));
							
		break;
		
		case 15:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , KatakanaL15 ))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,  HiraganaL15  ))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 , concat ( RomajiL14 , RomajiL15 ))))))))))))));
							
		break;

		case 16:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 ,  KatakanaL16	)))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   , HiraganaL16)))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 , concat ( RomajiL14 ,concat ( RomajiL15 , RomajiL16  )))))))))))))));


							
		break;
		
		case 17:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16, KatakanaL17 ))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, HiraganaL17))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 , concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  , RomajiL17  ))))))))))))))));
							
		break;
		
		case 18:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 ,  KatakanaL18)))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17, HiraganaL18)))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 , RomajiL18 )))))))))))))))));
							
		break;
		
		case 19:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, KatakanaL19 ))))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,  HiraganaL19))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 , RomajiL19 ))))))))))))))))));

							
		break;
		
		case 20:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, concat( KatakanaL19 , KatakanaL20 )))))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,concat(  HiraganaL19, HiraganaL20)))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 ,concat ( RomajiL19 , RomajiL20)))))))))))))))))));
							
		break;
		
		case 21:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat( KatakanaL3 , concat( KatakanaL4,  concat( KatakanaL5 , concat( KatakanaL6 , concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, concat( KatakanaL19 , concat( KatakanaL20 , KatakanaL21   ))))))))))))))))))));		
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,concat(  HiraganaL19,concat(  HiraganaL20, HiraganaL21 ))))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 ,concat ( RomajiL19 ,concat ( RomajiL20 , RomajiL21))))))))))))))))))));
							
		break;
		
		case 22:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat(KatakanaL3 , concat(KatakanaL4, concat( KatakanaL5 , concat( KatakanaL6 ,concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, concat( KatakanaL19 , concat( KatakanaL20 , concat( KatakanaL21 , KatakanaL22  )))))))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,concat(  HiraganaL19,concat(  HiraganaL20,concat(  HiraganaL21 , HiraganaL22)))))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 ,concat ( RomajiL19 ,concat ( RomajiL20 ,concat ( RomajiL21  ,RomajiL22)))))))))))))))))))));
							
		break;
		
		case 23:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat(KatakanaL3 , concat(KatakanaL4, concat( KatakanaL5 , concat( KatakanaL6 ,concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, concat( KatakanaL19 , concat( KatakanaL20 , concat( KatakanaL21 , KatakanaL22  )))))))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,concat(  HiraganaL19,concat(  HiraganaL20,concat(  HiraganaL21 , HiraganaL22)))))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 ,concat ( RomajiL19 ,concat ( RomajiL20 ,concat ( RomajiL21  ,RomajiL22)))))))))))))))))))));
							
		break;
		
		case 24:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat(KatakanaL3 , concat(KatakanaL4, concat( KatakanaL5 , concat( KatakanaL6 ,concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, concat( KatakanaL19 , concat( KatakanaL20 , concat( KatakanaL21 , KatakanaL22  )))))))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,concat(  HiraganaL19,concat(  HiraganaL20,concat(  HiraganaL21 , HiraganaL22)))))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 ,concat ( RomajiL19 ,concat ( RomajiL20 ,concat ( RomajiL21  ,RomajiL22)))))))))))))))))))));
						
		break;

		case 25:
			KatakanaSelected = concat(KatakanaL1, concat( KatakanaL2 , concat(KatakanaL3 , concat(KatakanaL4, concat( KatakanaL5 , concat( KatakanaL6 ,concat( KatakanaL7 , concat( KatakanaL8 , concat( KatakanaL9 , concat( KatakanaL10, concat( KatakanaL11, concat( KatakanaL12 ,  concat( KatakanaL13 , concat( KatakanaL14 , concat( KatakanaL15 , concat( KatakanaL16 , concat( KatakanaL17 , concat( KatakanaL18, concat( KatakanaL19 , concat( KatakanaL20 , concat( KatakanaL21 , KatakanaL22  )))))))))))))))))))));
			HiraganaSelected = concat(HiraganaL1, concat( HiraganaL2 , concat(HiraganaL3 , concat(HiraganaL4, concat( HiraganaL5 , concat( HiraganaL6 ,concat( HiraganaL7   ,concat(  HiraganaL8,concat(  HiraganaL9   , concat(  HiraganaL10, concat(  HiraganaL11   ,concat(  HiraganaL12   ,concat(  HiraganaL13   ,concat(  HiraganaL14  ,concat(  HiraganaL15   ,concat(  HiraganaL16, concat(  HiraganaL17   ,concat(  HiraganaL18,concat(  HiraganaL19,concat(  HiraganaL20,concat(  HiraganaL21 , HiraganaL22)))))))))))))))))))));
			RomajiSelected   = concat(RomajiL1 ,  concat( RomajiL2 ,   concat(RomajiL3   , concat(RomajiL4, concat(RomajiL5, concat(RomajiL6, concat(RomajiL7 ,concat ( RomajiL8 ,concat ( RomajiL9 ,concat ( RomajiL10  ,concat ( RomajiL11 ,concat ( RomajiL12 ,concat ( RomajiL13 ,concat ( RomajiL14 ,concat ( RomajiL15 ,concat ( RomajiL16  ,concat ( RomajiL17 ,concat ( RomajiL18 ,concat ( RomajiL19 ,concat ( RomajiL20 ,concat ( RomajiL21  ,RomajiL22)))))))))))))))))))));
							
		break;

	}

}

function getKatakanaDictionaryLevel(l:int){
		
	switch (l){
		case 1 :
			
			return KatakanaL1;
							
		break;

		case 2 :
			
			return KatakanaL2;
							
		break;

		case 3:
			
			return KatakanaL3;
					
		break;
		
		case 4:
			
			return KatakanaL4;
			
		break;
		
		case 5:
			
			return KatakanaL5;
					
		break;
		
		case 6:
			return KatakanaL6;
					
		break;
		
		case 7:
			return KatakanaL7;
			
		break;
		
		case 8:
			return KatakanaL8;
					
									
		break;
		
		case 9:
			return KatakanaL9;
							
		break;
		
		case 10:
			return KatakanaL10;
							
		break;

		case 11:
			return KatakanaL11;
							
		break;
		
		case 12:
			return KatakanaL12;
							
		break;
		
		case 13:
			return KatakanaL13;
							
		break;
		
		case 14:
			return KatakanaL14;
							
		break;
		
		case 15:
			return KatakanaL15;
							
		break;

		case 16:
			return KatakanaL16;
							
		break;
		
		case 17:
			return KatakanaL17;
							
		break;
		
		case 18:
			return KatakanaL18;
							
		break;
		
		case 19:
			return KatakanaL19;
							
		break;
		
		case 20:
			return KatakanaL20;
							
		break;
		
		case 21:
			return KatakanaL21;
							
		break;
		
		case 22:
			return KatakanaL22;
							
		break;
		

	}

}

/*function selectedDictionaryLevel(){
		
	switch (level){
		case 1 :
			
			KatakanaSelected = KatakanaL1;
			HiraganaSelected = HiraganaL1;
			RomajiSelected = RomajiL1;
							
		break;

		case 2 :
			
			KatakanaSelected = KatakanaL2;
			HiraganaSelected = HiraganaL2;
			RomajiSelected = RomajiL2;
							
		break;

		case 3:
			
			KatakanaSelected = KatakanaL3;
			HiraganaSelected = HiraganaL3;
			RomajiSelected = RomajiL3;
					
		break;
		
		case 4:
			
			KatakanaSelected = KatakanaL4;
			HiraganaSelected = HiraganaL4;
			RomajiSelected = RomajiL4;
			
		break;
		
		case 5:
			
			KatakanaSelected = KatakanaL5;
			HiraganaSelected = HiraganaL5;
			RomajiSelected = RomajiL5;
					
		break;
		
		case 6:
			KatakanaSelected = KatakanaL6;
			HiraganaSelected = HiraganaL6;
			RomajiSelected = RomajiL6;
					
		break;
		
		case 7:
			KatakanaSelected = KatakanaL7;
			HiraganaSelected = HiraganaL7;
			RomajiSelected = RomajiL7;
			
		break;
		
		case 8:
			KatakanaSelected = KatakanaL8;
			HiraganaSelected = HiraganaL8;
			RomajiSelected = RomajiL8;
					
									
		break;
		
		case 9:
			KatakanaSelected = KatakanaL9;
			HiraganaSelected = HiraganaL9;
			RomajiSelected = RomajiL9;
							
		break;
		
		case 10:
			KatakanaSelected = KatakanaL10;
			HiraganaSelected = HiraganaL10;
			RomajiSelected = RomajiL10;
							
		break;

		case 11:
			KatakanaSelected = KatakanaL11;
			HiraganaSelected = HiraganaL11;
			RomajiSelected = RomajiL11;
							
		break;
		
		case 12:
			KatakanaSelected = KatakanaL12;
			HiraganaSelected = HiraganaL12;
			RomajiSelected = RomajiL12;
							
		break;
		
		case 13:
			KatakanaSelected = KatakanaL13;
			HiraganaSelected = HiraganaL13;
			RomajiSelected = RomajiL13;
							
		break;
		
		case 14:
			KatakanaSelected = KatakanaL14;
			HiraganaSelected = HiraganaL14;
			RomajiSelected = RomajiL14;
							
		break;
		
		case 15:
			KatakanaSelected = KatakanaL15;
			HiraganaSelected = HiraganaL15;
			RomajiSelected = RomajiL15;
							
		break;

		case 16:
			KatakanaSelected = KatakanaL16;
			HiraganaSelected = HiraganaL16;
			RomajiSelected = RomajiL16;
							
		break;
		
		case 17:
			KatakanaSelected = KatakanaL17;
			HiraganaSelected = HiraganaL17;
			RomajiSelected = RomajiL17;
							
		break;
		
		case 18:
			KatakanaSelected = KatakanaL18;
			HiraganaSelected = HiraganaL18;
			RomajiSelected = RomajiL18;
							
		break;
		
		case 19:
			KatakanaSelected = KatakanaL19;
			HiraganaSelected = HiraganaL19;
			RomajiSelected = RomajiL19;
							
		break;
		
		case 20:
			KatakanaSelected = KatakanaL20;
			HiraganaSelected = HiraganaL20;
			RomajiSelected = RomajiL20;
							
		break;
		
		case 21:
			KatakanaSelected = KatakanaL21;
			HiraganaSelected = HiraganaL21;
			RomajiSelected = RomajiL21;
							
		break;
		
		case 22:
			KatakanaSelected = KatakanaL22;
			HiraganaSelected = HiraganaL22;
			RomajiSelected = RomajiL22;
							
		break;
		
		case 23:
							
		break;
		
		case 24:
						
		break;

		case 25:
							
		break;

	}

}
*/


