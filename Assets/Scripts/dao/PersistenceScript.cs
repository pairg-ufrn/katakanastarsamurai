﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;


public class PersistenceScript : MonoBehaviour {

	private BinaryFormatter bf = new BinaryFormatter ();
	private FileStream file = null;
	private List<LevelData> levelList = null;
	
	// Use this for initialization
	void start () {
		createLevelData ();
		levelList = getLevelDataList ();
	}

	public LevelData getLevelData(int level){
		if(levelList == null){
			levelList = getLevelDataList ();
		}
		return levelList[level];
	}


	public List<LevelData> getLevelDataList(){

		if (!File.Exists (Application.persistentDataPath + "/levelDate.dat")) {
			createLevelData();
		}

		if(levelList == null){
			file = File.Open(Application.persistentDataPath+ "/levelDate.dat", FileMode.Open);

			levelList = (List<LevelData>)bf.Deserialize(file);
			
			file.Close ();
		}

		return levelList;
		
	}

	public void clearedLevelData(int level){
		
		if (!File.Exists (Application.persistentDataPath + "/levelDate.dat")) {
			createLevelData();
		}
		else{
			file = File.Open(Application.persistentDataPath+ "/levelDate.dat", FileMode.Open);
				
			List<LevelData> lds = (List<LevelData>)bf.Deserialize(file);

			lds[level].state = LevelData.states.cleared;

			bf.Serialize(file, lds);

			file.Close ();
		}
				
	}

	public void closedLevelData(int level){
		
		if (!File.Exists (Application.persistentDataPath + "/levelDate.dat")) {
			createLevelData();
		}
		else{
			file = File.Open(Application.persistentDataPath+ "/levelDate.dat", FileMode.Open);
			
			List<LevelData> lds = (List<LevelData>)bf.Deserialize(file);
			
			lds[level].state = LevelData.states.closed;
			
			bf.Serialize(file, lds);

			file.Close ();
		}
		
		
	}

	public void fullyClearedLevelData(int level){
		
		if (!File.Exists (Application.persistentDataPath + "/levelDate.dat")) {
			createLevelData();
		}
		else{
			file = File.Open(Application.persistentDataPath+ "/levelDate.dat", FileMode.Open);
			
			List<LevelData> lds = (List<LevelData>)bf.Deserialize(file);
			
//			lds[level].state = LevelData.states.fully_cleared;

			bf.Serialize(file, lds);
			
			file.Close ();
		}
		
		
	}
	public void availableLevelData(int level){
		
		if (!File.Exists (Application.persistentDataPath + "/levelDate.dat")) {
			createLevelData();
		}
		else{
			file = File.Open(Application.persistentDataPath+ "/levelDate.dat", FileMode.Open);
			
			List<LevelData> lds = (List<LevelData>)bf.Deserialize(file);
			
			lds[level].state = LevelData.states.available;

			bf.Serialize(file, lds);
			
			file.Close ();
		}
		
		
	}


	public void createLevelData(){

		if (!File.Exists (Application.persistentDataPath + "/levelDate.dat")){

			FileStream file = File.Create(Application.persistentDataPath+ "/levelDate.dat");

			LevelData ld = null;

			List<LevelData> lds = new List<LevelData>();

			for (int i = 0; i < 25; i++) {
				ld = new LevelData ();
				ld.index = i+1;
				ld.state = LevelData.states.closed;
				lds.Add(ld);
			}

			bf.Serialize (file, lds);

			file.Close ();
		}

	}
}


