﻿using UnityEngine;
using System.Collections;

public class ChangeRails : MonoBehaviour {

	public GameObject rails;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	



		if (gameObject.GetComponent<AnimateScript> ().turn) {
			gameObject.GetComponent<Animator>().SetBool("right",true);
			rails.transform.localPosition = new Vector2 (2f,0);
		} else {
			gameObject.GetComponent<Animator>().SetBool("right",false);
			rails.transform.localPosition = new Vector2 (2f,0);
		}

	}
}
