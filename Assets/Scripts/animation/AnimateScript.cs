﻿using UnityEngine;
using System.Collections;

public class AnimateScript : MonoBehaviour {

	public bool turn = true;
	private int ang = 60;

	public float speed = 0.1f;

	public float force = 300;

	public float high = 0;

	public Vector3 position1;
	public Vector3 position2;

	// Use this for initialization
	void Start () {
		 position1 = new Vector3(25f,10f,0f);
		 position2  = new Vector3(-25f,-10f,0f);

	}
	
	// Update is called once per frame
	void Update () {



		if ( position1.x > transform.position.x && turn){
			GetComponent<Rigidbody2D>().AddForce(Vector2.right * force);
			transform.eulerAngles = Vector3.right;			
		}
		else{
			turn = false;


		}
		
		if (position2.x < transform.position.x && !turn){

			GetComponent<Rigidbody2D>().AddForce(Vector2.right * -force);

			transform.eulerAngles = (Vector3.up * 180) + (Vector3.right * 360) ;

		}
		else{
			turn = true;

		}


		if (transform.position.y < high ){
			GetComponent<Rigidbody2D>().AddForce(Vector2.up * (force+200));
		}



	}
}
