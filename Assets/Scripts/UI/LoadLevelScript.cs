﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class LoadLevelScript : MonoBehaviour {


	
	private BinaryFormatter bf = new BinaryFormatter ();
	private PersistenceScript ps;
	private RuntimePlatform platform = Application.platform;

	public LevelData ld;

	public void SetLevelData(LevelData ld){
		this.ld = ld;
	}

	// Use this for initialization
	void Start () {
		transform.FindChild("TextLevel").GetComponent<TextMesh>().text = ""+ld.index;

	}
	
	// Update is called once per frame
	void Update () {

		switch(ld.state){
			
		case LevelData.states.available:
			transform.GetComponent<Animator>().SetInteger("state",1);

			break;
			
		case LevelData.states.cleared:
			transform.GetComponent<Animator>().SetInteger("state",2);
			break;
			
		case LevelData.states.closed:
			transform.GetComponent<Animator>().SetInteger("state",3);
			break;
			
		case LevelData.states.one_star_cleared:
			transform.GetComponent<Animator>().SetInteger("state",4);
			break;
			
		case LevelData.states.two_star_cleared:
			transform.GetComponent<Animator>().SetInteger("state",5);
			break;
			
		case LevelData.states.three_star_cleared:
			transform.GetComponent<Animator>().SetInteger("state",6);
			break;
			
			
		}

		bool hitCollider ;

		if(ld.state != LevelData.states.closed){

			if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player){
				if(Input.touchCount > 0) {
					if(Input.GetTouch(0).phase == TouchPhase.Began){
						Vector2 touchPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
						hitCollider = gameObject.GetComponent<BoxCollider2D>().OverlapPoint(touchPosition);
						
						if(hitCollider){

							PlayerPrefs.SetInt(LevelData.KEY_LEVEL,ld.index);

							Application.LoadLevel(Settings.SCENE_GAME);
						}
					}
				}
			}else if(platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer || platform == RuntimePlatform.WindowsWebPlayer || platform == RuntimePlatform.WindowsPlayer){
				if (Input.GetMouseButtonDown(0))
				{
					Vector2 mousePosition  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					hitCollider = gameObject.GetComponent<BoxCollider2D>().OverlapPoint(mousePosition);
					
					Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    
						
					if(hitCollider){

						
						PlayerPrefs.SetInt(LevelData.KEY_LEVEL,ld.index);

						Application.LoadLevel(Settings.SCENE_GAME);
					}
				}
				
			}
		}
	}
}