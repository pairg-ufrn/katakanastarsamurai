﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

public class LoadModeScript : MonoBehaviour {

	public string mode;
	public string difficulty;

	private bool selected = false;

	public Sprite SpriteSelectorGameMode;

	public ModeSelectionController controller;

	private LanguageManager lm;

		private	string HE = Settings.MODE_HIRAGANA+Settings.DIFFICULTY_EASY;
		private	string HN = Settings.MODE_HIRAGANA+Settings.DIFFICULTY_NORMAL;
		private	string HH = Settings.MODE_HIRAGANA+Settings.DIFFICULTY_HARD;
		private	string RE = Settings.MODE_ROMAJI+Settings.DIFFICULTY_EASY;
		private	string RN = Settings.MODE_ROMAJI+Settings.DIFFICULTY_NORMAL;
		private	string RH = Settings.MODE_ROMAJI + Settings.DIFFICULTY_HARD;

	private RuntimePlatform platform = Application.platform;

	// Use this for initialization
	void Start () {
	
		lm = LanguageManager.Instance;

			

		

	}
	/*
											if (controller.GetComponent<ModeSelectionController> ().seleted != this) {
									
																PlayerPrefs.SetString (Settings.KEY_DIFFICULTY, difficulty);
																PlayerPrefs.SetString (Settings.KEY_MODE, mode);
																controller.GetComponent<ModeSelectionController> ().seleted = this;
														} else {
																if (difficulty == Settings.MODE_HIRAGANA) {
																		difficulty = Settings.MODE_ROMAJI;
																} else {
																		difficulty = Settings.MODE_HIRAGANA;
																}

																PlayerPrefs.SetString (Settings.KEY_DIFFICULTY, difficulty);
																PlayerPrefs.SetString (Settings.KEY_MODE, mode);
									
														}

												}

if (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA) {
									transform.FindChild ("TextMode").GetComponent<TextMesh> ().text = "あ";
									
								} else {
									
									transform.FindChild ("TextMode").GetComponent<TextMesh> ().text = "a";
								}


		 */
	// Update is called once per frame


		public void select(){


			//if (controller.GetComponent<ModeSelectionController> ().seleted != this) {
				
				PlayerPrefs.SetString (Settings.KEY_DIFFICULTY, difficulty);
				PlayerPrefs.SetString (Settings.KEY_MODE, mode);
				controller.GetComponent<ModeSelectionController> ().seleted = this;
			/*} else {
				if (mode == Settings.MODE_HIRAGANA) {
					mode = Settings.MODE_ROMAJI;
					transform.FindChild ("TextMode").GetComponent<TextMesh> ().text = "a";
				} else {
					mode = Settings.MODE_HIRAGANA;

					transform.FindChild ("TextMode").GetComponent<TextMesh> ().text = "あ";
				}
				
				PlayerPrefs.SetString (Settings.KEY_DIFFICULTY, difficulty);
				PlayerPrefs.SetString (Settings.KEY_MODE, mode);
				
			}*/

		}

	void Update () {
						
			bool hitCollider ;

			if (mode + difficulty == HE) {
				transform.GetComponent<Animator>().SetInteger("state",1);
				transform.FindChild ("TextMode").GetComponent<TextMesh>().text = "あ";
			}else if (mode+difficulty == HN){
				transform.GetComponent<Animator>().SetInteger("state",2);
				transform.FindChild ("TextMode").GetComponent<TextMesh>().text = "あ";
			}else if (mode+difficulty == HH){
				transform.GetComponent<Animator>().SetInteger("state",3);
				transform.FindChild ("TextMode").GetComponent<TextMesh> ().text = "あ";
			}else if (mode+difficulty == RE){
				transform.GetComponent<Animator>().SetInteger("state",4);
				transform.FindChild ("TextMode").GetComponent<TextMesh>().text = "a";
			}else if (mode+difficulty == RN){
				transform.GetComponent<Animator>().SetInteger("state",5);
				transform.FindChild ("TextMode").GetComponent<TextMesh>().text = "a";
			}else if (mode+difficulty == RH){
				transform.GetComponent<Animator>().SetInteger("state",6);
				transform.FindChild ("TextMode").GetComponent<TextMesh>().text = "a";
			}


			if (controller.GetComponent<ModeSelectionController> ().seleted == this) {
				transform.FindChild ("SpriteSelectorGameMode").GetComponent<SpriteRenderer> ().sprite = SpriteSelectorGameMode;
			} else {
				transform.FindChild ("SpriteSelectorGameMode").GetComponent<SpriteRenderer> ().sprite = null;
			}

				
			if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player){
					if(Input.touchCount > 0) {
						if(Input.GetTouch(0).phase == TouchPhase.Began){
							Vector2 touchPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
							hitCollider = gameObject.GetComponent<BoxCollider2D>().OverlapPoint(touchPosition);
							
							if(hitCollider){
								
								
								select();					
							
							}
						}
					}
			}else if(platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer || platform == RuntimePlatform.WindowsWebPlayer || platform == RuntimePlatform.WindowsPlayer){
					if (Input.GetMouseButtonDown(0))
					{
						Vector2 mousePosition  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
						hitCollider = gameObject.GetComponent<BoxCollider2D>().OverlapPoint(mousePosition);
						
						Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    
						
						if(hitCollider){
							
							select();			
							
						}
					
				}
			}


	
}
}
}