﻿namespace SmartLocalization
{
	using UnityEngine;
	using System.Collections.Generic;

public class LanguageMenuScript : MonoBehaviour {
	
	private List<SmartCultureInfo> availableLanguages;
	private LanguageManager thisLanguageManager;
	
	public GameObject optionMenu;

	private int screenWidth = Screen.width;
	private int screenHeight = Screen.height;
	
	private int windowWidth = 800;
	private int windowHeight = 600;
	private int windowX ;
	private int windowY ;

	void Start(){

		thisLanguageManager = LanguageManager.Instance;

		availableLanguages = thisLanguageManager.GetSupportedLanguages();

		windowX = ( screenWidth - windowWidth ) / 2;
		windowY = ( screenHeight - windowHeight ) / 2;
	}

	void OnGUI () {
		// Make a background box
			GUI.Box(new Rect(windowX,windowY,windowWidth,windowHeight), thisLanguageManager.GetTextValue("SplashScene.OptionsMenu.LabelMenu"));
		
		GUILayout.BeginHorizontal();
		
		GUILayout.BeginVertical();
		

		if(thisLanguageManager.CurrentlyLoadedCulture != null)
				GUI.Label( new Rect( windowX+10, windowY+20, 300, 40 ),  thisLanguageManager.GetTextValue("SplashScene.OptionsMenu.LanguageMenu.LabelLaguageCurrent") + " : " + thisLanguageManager.CurrentlyLoadedCulture.nativeName);

		int i = 0;
		foreach(SmartCultureInfo language in availableLanguages){
				i++;
			if(GUI.Button( new Rect( windowX+10, windowY+(40*i), 300, 30 ),language.nativeName)){
				thisLanguageManager.ChangeLanguage(language);
				PlayerPrefs.SetString(Settings.KEY_LANGUEGE, language.languageCode);
			}
		}

		if (GUI.Button( new Rect( windowX+10, windowY+230, 300, 30 ),  thisLanguageManager.GetTextValue("General.Exit") )){
			Instantiate(optionMenu , new Vector2(0,0), Quaternion.identity);
			GameObject.Destroy(this);
		}

		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		
	}
}
}