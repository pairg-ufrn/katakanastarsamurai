﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections.Generic;

public class OptionsMenuScript : MonoBehaviour {


		private List<SmartCultureInfo> availableLanguages;
		private LanguageManager thisLanguageManager;

		public GUISkin skin;

		private GUIStyle style;

		private GUIStyle styleb;

		public GameObject optionMenu;
		
		private int screenWidth = Screen.width;
		private int screenHeight = Screen.height;
		
		private int windowWidth = 640;
		private int windowHeight = 600;
		private int windowX ;
		private int windowY ;
		
		void Start(){

			style = skin.GetStyle ("PopupWindow");

			style.fontSize = 25;

			styleb = skin.GetStyle ("Button");
			
			styleb.fontSize = 25;

			thisLanguageManager = LanguageManager.Instance;
			
			availableLanguages = thisLanguageManager.GetSupportedLanguages();
			
			windowX = ( screenWidth - windowWidth ) / 2;
			windowY = ( screenHeight - windowHeight ) / 2;
		}
		
		void OnGUI () {
			// Make a background box
			GUI.Box(new Rect(windowX,windowY,windowWidth,windowHeight), "", style);
			
			GUILayout.BeginHorizontal();
			
			GUILayout.BeginVertical();
			
			
			if(thisLanguageManager.CurrentlyLoadedCulture != null)
				GUI.Label( new Rect( windowX+20, windowY+40, 600, 80 ),  thisLanguageManager.GetTextValue("SplashScene.OptionsMenu.LanguageMenu.LabelLaguageCurrent") + " : " + FirstCharToUpper(thisLanguageManager.CurrentlyLoadedCulture.nativeName), skin.GetStyle ("Label"));
			
			int i = 0;
			foreach(SmartCultureInfo language in availableLanguages){
				i++;
				string l = FirstCharToUpper(language.nativeName);

				if(GUI.Button( new Rect( windowX+20, windowY+(100*i), 600, 60 ),l , styleb)){
					thisLanguageManager.ChangeLanguage(language);
					PlayerPrefs.SetString(Settings.KEY_LANGUEGE, language.languageCode);
				}
			}
			
			if (GUI.Button( new Rect( windowX+20, windowY+500, 600, 60 ),  thisLanguageManager.GetTextValue("General.Exit"),  styleb )){
				GameObject.Destroy(this.gameObject);
			}
			
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			
		}

		public string FirstCharToUpper(string input)
		{
			string first = input.Substring (0, 1).ToUpper();
			string rest = input.Substring (1);
			return  first + rest;
		}

}
}