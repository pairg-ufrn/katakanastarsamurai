﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

	public class OptionsMenuScriptOld : MonoBehaviour {


		public GameObject languageMenu;

		private int screenWidth = Screen.width;
		private int screenHeight = Screen.height;
		
		private int windowWidth = 320;
		private int windowHeight = 280;
		private int windowX ;
		private int windowY ;

		private LanguageManager lm;

		void Start(){
			windowX = ( screenWidth - windowWidth ) / 2;
			windowY = ( screenHeight - windowHeight ) / 2;
		}

		void OnGUI () {
			// Make a background box

			lm = LanguageManager.Instance;

			GUI.Box(new Rect(windowX,windowY,windowWidth,windowHeight), lm.GetTextValue("SplashScene.OptionsMenu.LabelMenu"));
			
			GUILayout.BeginHorizontal();

			GUILayout.BeginVertical();

			string labeButtonDifficulty;

			if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_EASY){
				labeButtonDifficulty = lm.GetTextValue("SplashScene.OptionsMenu.LabelDifficulty")+ " : "+ lm.GetTextValue("SplashScene.OptionsMenu.buttonDifficultyEasy");
			}
			else if (PlayerPrefs.GetString (Settings.KEY_DIFFICULTY) == Settings.DIFFICULTY_HARD){
				labeButtonDifficulty = lm.GetTextValue("SplashScene.OptionsMenu.LabelDifficulty")+ " : "+ lm.GetTextValue("SplashScene.OptionsMenu.buttonDifficultyHard");
			}
			else {
				labeButtonDifficulty = lm.GetTextValue("SplashScene.OptionsMenu.LabelDifficulty")+ " : "+ lm.GetTextValue("SplashScene.OptionsMenu.buttonDifficultyNormal");
			}

			
			GUI.Label( new Rect( windowX+10, windowY+20, 120, 40 ), labeButtonDifficulty);
			
			if ( GUI.Button( new Rect( windowX+10, windowY+70, 120, 30 ), lm.GetTextValue("SplashScene.OptionsMenu.buttonDifficultyEasy") ) )
			{
				PlayerPrefs.SetString(Settings.KEY_DIFFICULTY,Settings.DIFFICULTY_EASY);
				
			}
			if ( GUI.Button( new Rect( windowX+10, windowY+110, 120, 30 ), lm.GetTextValue("SplashScene.OptionsMenu.buttonDifficultyNormal")) )
			{
				PlayerPrefs.SetString(Settings.KEY_DIFFICULTY,Settings.DIFFICULTY_NORMAL);
			}
			if ( GUI.Button( new Rect( windowX+10, windowY+150, 120, 30 ), lm.GetTextValue("SplashScene.OptionsMenu.buttonDifficultyHard") ) )
			{
				PlayerPrefs.SetString(Settings.KEY_DIFFICULTY,Settings.DIFFICULTY_HARD);
			}
			
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();

			string labeButtonMode;

			if (PlayerPrefs.GetString (Settings.KEY_MODE) == Settings.MODE_HIRAGANA){
				labeButtonMode = lm.GetTextValue ("SplashScene.OptionsMenu.LabelModeGame") + " : " + lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameHiragana");
			}
			else{
				labeButtonMode = lm.GetTextValue ("SplashScene.OptionsMenu.LabelModeGame") + " : " + lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameRomaji");
			}

			GUI.Label(new Rect( windowX+150, windowY+20, 140, 40 ),labeButtonMode);
			
			if ( GUI.Button( new Rect( windowX+150, windowY+70, 160, 30 ), lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameHiragana") ) )
			{
				PlayerPrefs.SetString(Settings.KEY_MODE,Settings.MODE_HIRAGANA);
				
			}    
			
			if ( GUI.Button( new Rect( windowX+150, windowY+110, 160, 30 ), lm.GetTextValue ("SplashScene.OptionsMenu.buttonModeGameRomaji") ) )
			{
				PlayerPrefs.SetString(Settings.KEY_MODE,Settings.MODE_ROMAJI);
				
			}
			
			if (GUI.Button( new Rect( windowX+10, windowY+190, 300, 30 ), lm.GetTextValue ("SplashScene.OptionsMenu.buttonLanguage") ))
			{
				Instantiate(languageMenu , new Vector2(0,0), Quaternion.identity);

				GameObject.Destroy(this.gameObject);
			}

			if (GUI.Button( new Rect( windowX+10, windowY+230, 300, 30 ), lm.GetTextValue ("General.Exit") ))
			{
				GameObject.Destroy(this.gameObject);
			}
			
			
			GUILayout.EndVertical();
			GUILayout.EndHorizontal(); 
		}
	}
}