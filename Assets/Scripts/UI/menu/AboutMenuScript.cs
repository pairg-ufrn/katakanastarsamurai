﻿using UnityEngine;
using System.Collections;

public class AboutsMenuScript : MonoBehaviour {



	private int screenWidth = Screen.width;
	private int screenHeight = Screen.height;
	
	private int windowWidth = 320;
	private int windowHeight = 280;
	private int windowX ;
	private int windowY ;

	void Start(){
		windowX = ( screenWidth - windowWidth ) / 2;
		windowY = ( screenHeight - windowHeight ) / 2;
	}

	void OnGUI () {
		// Make a background box
		GUI.Box(new Rect(windowX,windowY,windowWidth,windowHeight), "Loader Menu");
		
		GUILayout.BeginHorizontal();
		
		GUILayout.BeginVertical();
		
	
		GUI.TextArea (new Rect (windowX + 10, windowY + 20, 300, 30), "XML File ...");


		if (GUI.Button( new Rect( windowX+10, windowY+230, 300, 30 ), "Sair" ))
		{
			GameObject.Destroy(this);
		}
		
		
		GUILayout.EndVertical();
		GUILayout.EndHorizontal(); 
	}
}