﻿namespace SmartLocalization
{
using UnityEngine;
using System.Collections;

public class PauseMenuScript : MonoBehaviour {


	private int screenWidth = Screen.width;
	private int screenHeight = Screen.height;

	public GUISkin skin;
		
	private GUIStyle style;
	
	private GUIStyle styleb;

	private int windowWidth = 640;
	private int windowHeight = 600;
	private int windowX ;
	private int windowY ;

	private LanguageManager lm;

	void Start(){
		windowX = ( screenWidth - windowWidth ) / 2;
		windowY = ( screenHeight - windowHeight ) / 2;
		
			style = skin.GetStyle ("PopupWindow");
			
			style.fontSize = 25;
			
			styleb = skin.GetStyle ("Button");
			
			styleb.fontSize = 25;


	}

	void OnGUI () {

			lm = LanguageManager.Instance;
			
			lm.ChangeLanguage (PlayerPrefs.GetString (Settings.KEY_LANGUEGE));

		if(PlayerPrefs.GetInt("paused") == 1){
			// Make a background box
				GUI.Box(new Rect(windowX,windowY,windowWidth,windowHeight), "", style);
			
			GUILayout.BeginHorizontal();
			
			GUILayout.BeginVertical();

			
	//		GUI.Label( new Rect( windowX+10, windowY+20, 120, 40 ), lm.GetTextValue("MenuScene.labelState.available"));

				if (GUI.Button( new Rect( windowX+20, windowY+100, 600, 60 ), lm.GetTextValue("MenuPause.buttonReturn"), styleb )){
				GameObject.Find("PauseController").GetComponent<PauseScript>().offPause();
			}



			if (GUI.Button( new Rect( windowX+20, windowY+200, 600, 60 ), lm.GetTextValue("MenuPause.buttonBackLevelMenu"), styleb )){
					Application.LoadLevel("SelectionLevelScene");
			}

				if (GUI.Button( new Rect( windowX+20, windowY+300, 600, 60 ),lm.GetTextValue("MenuPause.buttonResartLevel"), styleb )){
				Application.LoadLevel("JSGameScene");
			}

			if (GUI.Button( new Rect( windowX+20, windowY+400, 600, 60 ), lm.GetTextValue("MenuPause.buttonExitGame"), styleb )){
				Application.Quit(); 
			}
			
			
			GUILayout.EndVertical();
			GUILayout.EndHorizontal(); 
		}
	}
}
}