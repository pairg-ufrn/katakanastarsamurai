﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {

	private bool paused;

	// Use this for initialization
	void Start () {
		paused = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.P) ) {
			paused = !paused;
		}

		if (paused) {
			Time.timeScale = 0;
			PlayerPrefs.SetInt("paused",1);
		} else {
			Time.timeScale = 1;
			PlayerPrefs.SetInt("paused",0);
		}
	}

	public void onPause(){

		paused = true;
	}

	public void offPause(){
		
		paused = false;
	}


}
