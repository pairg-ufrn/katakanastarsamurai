﻿using UnityEngine;
using System.Collections;

public class FillScreenSquard : MonoBehaviour {

	public float lSize = 7.0f; 


	void Update()
	{
		Camera lCamera = Camera.main;
		
		if(lCamera.orthographic)
		{
			float lSizeY = lCamera.orthographicSize * 2.0f;
			float lSizeX = lSizeY *lCamera.aspect;

			transform.localScale = new Vector3(lSizeY/lSize,lSizeY/lSize, 1);
		}
		else
		{
			float lPosition = (lCamera.nearClipPlane + 0.01f);
			transform.position = lCamera.transform.position + lCamera.transform.forward * lPosition;
			transform.LookAt (lCamera.transform);
			transform.Rotate (90.0f, 0.0f, 0.0f);
			
			float h = (Mathf.Tan(lCamera.fov*Mathf.Deg2Rad*0.5f)*lPosition*2f) /lSize;
			transform.localScale = new Vector3(h*lCamera.aspect, h, 1.0f);
		}
	}
}
