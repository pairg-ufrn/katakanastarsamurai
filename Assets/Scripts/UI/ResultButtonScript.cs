﻿using UnityEngine;
using System.Collections;




public class ResultButtonScript : ButtonGoToScript {




	public int level = 0;

	private bool disabled = false;

	public void disable(){
		transform.FindChild("spriteButton").GetComponent<SpriteRenderer> ().sprite = null;
		GameObject.Destroy(GetComponent<BoxCollider2D>());
		disabled = true;
	}

	public override void hit(){
		if (!disabled) {
				switch (goToScene) {
	
				case Scene.MainMenuScene:
					Application.LoadLevel (Settings.SCENE_MAIN_MENU);
				break;
	
				case Scene.GameScene:
					PlayerPrefs.SetInt(LevelData.KEY_LEVEL, level);
					Application.LoadLevel (Settings.SCENE_GAME);
				break;
	
				case Scene.ResultScene:
					Application.LoadLevel (Settings.SCENE_RESULT);
				break;
	
				case Scene.SelectionLevelScene:
					Application.LoadLevel (Settings.SCENE_SELECTION_LEVEL);
				break;
	
				case Scene.SelectionModeScene:
					Application.LoadLevel (Settings.SCENE_SELECTION_MODE);
				break;
	
				case Scene.MenuOption :
					if (GameObject.Find("OptionsGame(Clone)") == null){
						Instantiate(menuOption, new Vector2(0,0), Quaternion.identity);
					}
				break;
				
				case Scene.MenuPause :
					if (GameObject.Find("PauseDisplay(Clone)") == null){
						Instantiate(menuPause, new Vector2(0,0), Quaternion.identity);
					}
				break;
	
	
				}
		}
	}



}
