﻿using UnityEngine;
using System.Collections;




public class ButtonGoToScript : MonoBehaviour {

	private RuntimePlatform platform = Application.platform;

	public enum Scene {MainMenuScene, SelectionModeScene, SelectionModeAppedScene, SelectionLevelScene, ResultScene, GameScene, MenuOption, MenuPause, AboutScene };	

	public Scene goToScene;

	public GameObject menuOption;

	public GameObject menuPause;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
		bool hitCollider ;
		
			
			if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer || platform == RuntimePlatform.WP8Player){
				if(Input.touchCount > 0) {
					if(Input.GetTouch(0).phase == TouchPhase.Began){
						Vector2 touchPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
						hitCollider = gameObject.GetComponent<BoxCollider2D>().OverlapPoint(touchPosition);
						
						if(hitCollider){
							hit();
						}
					}
				}
		}else if(platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer ||  platform == RuntimePlatform.WindowsPlayer){
				if (Input.GetMouseButtonDown(0))
				{
					Vector2 mousePosition  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					hitCollider = gameObject.GetComponent<BoxCollider2D>().OverlapPoint(mousePosition);
					
					Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    
					
					if(hitCollider){
						hit();
					}
				}
				
			}
		}



	public virtual void hit(){
		switch(goToScene){

			case Scene.MainMenuScene :
				Application.LoadLevel(Settings.SCENE_MAIN_MENU);
			break;

			case Scene.GameScene :
				Application.LoadLevel(Settings.SCENE_GAME);
			break;

			case Scene.ResultScene :
				Application.LoadLevel(Settings.SCENE_RESULT);
			break;

			case Scene.SelectionLevelScene :
				Application.LoadLevel(Settings.SCENE_SELECTION_LEVEL);
			break;

			case Scene.SelectionModeScene :
				Application.LoadLevel(Settings.SCENE_SELECTION_MODE);
			break;

			case Scene.SelectionModeAppedScene :
				Application.LoadLevel(Settings.SCENE_SELECTION_MODEAPPED);
			break;

			case Scene.AboutScene :
				Application.LoadLevel(Settings.SCENE_ABOUT);
			break;
				
			case Scene.MenuOption :
				if (GameObject.Find("OptionsGame(Clone)") == null){
					Instantiate(menuOption, new Vector2(0,0), Quaternion.identity);
				}
			break;

			case Scene.MenuPause :
				if (GameObject.Find("PauseDisplay(Clone)") == null){
					Instantiate(menuPause, new Vector2(0,0), Quaternion.identity);
				}
			break;


		}

	}


}
