﻿using UnityEngine;
using System.Collections;

public class FillScreenPlane : MonoBehaviour {

	public float lWSize = 12.78f; 

	public float lHSize = 7.1f; 

	void Update()
	{
		Camera lCamera = Camera.main;
		
		if(lCamera.orthographic)
		{
			float lSizeY = lCamera.orthographicSize * 2.0f;
			float lSizeX = lSizeY *lCamera.aspect;

			transform.localScale = new Vector3(lSizeX/lWSize,lSizeY/lHSize, 1);
		}
		else
		{
			float lPosition = (lCamera.nearClipPlane + 0.01f);
			transform.position = lCamera.transform.position + lCamera.transform.forward * lPosition;
			transform.LookAt (lCamera.transform);
			transform.Rotate (90.0f, 0.0f, 0.0f);
			
			float h = (Mathf.Tan(lCamera.fov*Mathf.Deg2Rad*0.5f)*lPosition*2f) /lWSize;
			transform.localScale = new Vector3(h*lCamera.aspect, h, 1.0f);
		}
	}
}
